//
//  +UserDefaults.swift
//  CoffeeLabBL
//
//  Created by Diego Espinoza on 12/12/22.
//

import Foundation

extension UserDefaults {
    enum Keys: String {
        case temporalMyCoffee
    }
    
    static func save<T: Codable>(_ data: T, usingKey key: Keys) {
        if let encoded = try? JSONEncoder().encode(data) {
            let defaults = UserDefaults.standard
            defaults.set(encoded, forKey: key.rawValue)
        }
    }

    static func fetch<T: Codable>(usingKey key: Keys) -> T? {
        let defaults = UserDefaults.standard
        if let data = defaults.object(forKey: key.rawValue) as? Data {
            return try? JSONDecoder().decode(T.self, from: data)
        }
        return nil
    }
}

//
//  COFMyCoffeeError.swift
//  CoffeeLabBL
//
//  Created by Diego Espinoza on 13/12/22.
//

import Foundation

public enum COFMyCoffeeError: LocalizedError {
    case coffeeDoesNotExist
    
    public var errorDescription: String? {
        switch self {
        case .coffeeDoesNotExist:
            return "Could not find the coffee in the database"
        }
    }
}

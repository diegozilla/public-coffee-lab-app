//
//  MyCoffee.swift
//  CoffeeLabBL
//
//  Created by Diego Espinoza on 11/12/22.
//

import Foundation

public struct COFMyCoffee: Codable, Sendable {
    public var uuid = UUID()
    public let name: String
    public let roastType: String
    public let photo: Data?
    public let description: String?
    public let origin: String?
    public let variety: String?
    public var isFavorite: Bool
    
    public init(name: String,
                roastType: String,
                origin: String?,
                variety: String?,
                description: String?,
                photo: Data?,
                isFavorite: Bool) {
        self.name = name
        self.roastType = roastType
        self.origin = origin
        self.variety = variety
        self.description = description
        self.photo = photo
        self.isFavorite = isFavorite
    }
    
    init(coffeeEntity: COFCoffeeEntity) {
        self.uuid = coffeeEntity.uuid
        self.name = coffeeEntity.name
        self.origin = coffeeEntity.origin
        self.variety = coffeeEntity.variety
        self.description = coffeeEntity.detail
        self.roastType = coffeeEntity.roast
        self.isFavorite = coffeeEntity.isFavorite
        self.photo = coffeeEntity.photo
    }
    
    #if DEBUG

    static func stub() -> [COFMyCoffee] {
        [
            .init(name: "Peruvian Coffee",
                  roastType: "Light",
                  origin: "Cajamarca",
                  variety: "Caturra",
                  description: "This is a peruvian coffee",
                  photo: nil,
                  isFavorite: false),
            .init(name: "Colombian Coffee",
                  roastType: "Medium",
                  origin: "Bogota",
                  variety: "Bourbon",
                  description: "This is a colombian coffee",
                  photo: nil,
                  isFavorite: true),
            .init(name: "Etiopian Coffee",
                  roastType: "Dark",
                  origin: nil,
                  variety: "Caturra",
                  description: "This is an etiopian coffee",
                  photo: nil,
                  isFavorite: false)
        ]
    }

    #endif
}

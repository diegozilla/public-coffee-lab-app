//
//  MyCoffeeRepositoryService.swift
//  CoffeeLabBL
//
//  Created by Diego Espinoza on 11/12/22.
//

import Foundation

public protocol COFMyCoffeeRepositoryServiceType {
    func fetch(with id: UUID) async throws -> COFMyCoffee
    func fetch() async throws -> [COFMyCoffee]
    func save(coffee: COFMyCoffee) async throws
    func update(coffee: COFMyCoffee, with id: UUID) async throws
}

public actor COFMyCoffeeRepositoryService: COFMyCoffeeRepositoryServiceType {
    private var persistenceController = COFRepositoryController()
    
    public init() {}
    
    public func fetch(with id: UUID) async throws -> COFMyCoffee {
        guard let first = try await fetchEntity(with: id) else {
            throw COFMyCoffeeError.coffeeDoesNotExist
        }
        return COFMyCoffee(coffeeEntity: first)
    }
    
    public func fetch() async throws -> [COFMyCoffee] {
        let fetchCoffee = COFCoffeeEntity.fetchRequest()
        let results = try persistenceController.managedContext.fetch(fetchCoffee)
        return results.map(COFMyCoffee.init)
    }
    
    public func save(coffee: COFMyCoffee) async throws {
        let newCoffee = COFCoffeeEntity(context: persistenceController.managedContext)
        newCoffee.uuid = coffee.uuid
        newCoffee.name = coffee.name
        newCoffee.roast = coffee.roastType
        newCoffee.detail = coffee.description
        newCoffee.variety = coffee.variety
        newCoffee.origin = coffee.origin
        newCoffee.isFavorite = coffee.isFavorite
        newCoffee.photo = coffee.photo
        persistenceController.saveContext()
    }
    
    public func update(coffee: COFMyCoffee, with id: UUID) async throws {
        let result = try await fetchEntity(with: id)
        guard let newCoffee = result else {
            throw COFMyCoffeeError.coffeeDoesNotExist
        }
        newCoffee.uuid = coffee.uuid
        newCoffee.name = coffee.name
        newCoffee.roast = coffee.roastType
        newCoffee.detail = coffee.description
        newCoffee.variety = coffee.variety
        newCoffee.origin = coffee.origin
        newCoffee.isFavorite = coffee.isFavorite
        newCoffee.photo = coffee.photo
        persistenceController.saveContext()
    }
    
    private func fetchEntity(with id: UUID) async throws -> COFCoffeeEntity? {
        let fetchCoffee = COFCoffeeEntity.fetchRequest()
        fetchCoffee.predicate = NSPredicate(format: "uuid == %@", id.uuidString)
        let result = try persistenceController.managedContext.fetch(fetchCoffee)
        return result.first
    }
}

//
//  COFCoffeeEntity.swift
//  CoffeeLabBL
//
//  Created by Diego Espinoza on 13/12/22.
//

import Foundation
import CoreData

class COFCoffeeEntity: NSManagedObject {
    @NSManaged var uuid: UUID
    @NSManaged var name: String
    @NSManaged var roast: String
    @NSManaged var variety: String?
    @NSManaged var detail: String?
    @NSManaged var origin: String?
    @NSManaged var photo: Data?
    
    @NSManaged var isFavorite: Bool

    @nonobjc class func fetchRequest() -> NSFetchRequest<COFCoffeeEntity> {
        return NSFetchRequest<COFCoffeeEntity>(entityName: "COFCoffeeEntity")
    }
}

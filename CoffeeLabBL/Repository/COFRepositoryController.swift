//
//  COFRepositoryController.swift
//  CoffeeLabBL
//
//  Created by Diego Espinoza on 13/12/22.
//

import Foundation
import CoreData

final class COFRepositoryController {
    lazy var persistentContainer: NSPersistentContainer = {
        let bundle = Bundle(for: COFRepositoryController.self)
        guard let modelURL = bundle.url(forResource: "RepositoryDataModel",
                                        withExtension: "momd") else {
            fatalError("Unable to get modelURL")
        }
        
        guard let model = NSManagedObjectModel(contentsOf: modelURL) else {
            fatalError("Unable to get model")
        }
        
        let container = NSPersistentContainer(name: "RepositoryDataModel", managedObjectModel: model)
        container.loadPersistentStores { description, error in
            if let error = error {
                fatalError("Unable to load persistent stores: \(error)")
            }
        }
        return container
    }()
    
    var managedContext: NSManagedObjectContext {
        persistentContainer.viewContext
    }
    
    func saveContext() {
        guard managedContext.hasChanges else {
            print("no changes executed in managedContext")
            return
        }
        do {
            try managedContext.save()
        } catch {
            print("save context failed")
        }
    }
}

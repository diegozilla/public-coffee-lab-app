//
//  Spacing.swift
//  CoffeeLabUI
//
//  Created by Diego Espinoza on 8/12/22.
//

import UIKit

public struct COFSpacing {
    /// value: 4
    public static let tiny: CGFloat = 4
    /// value: 8
    public static let small: CGFloat = 8
    /// value: 12
    public static let xSmall: CGFloat = 12
    /// value: 16
    public static let medium: CGFloat = 16
    /// value: 24
    public static let xMedium: CGFloat = 24
    /// value: 32
    public static let large: CGFloat = 32
    /// value: 48
    public static let xLarge: CGFloat = 48
    /// value: 64
    public static let xxLarge: CGFloat = 64
}

extension CGFloat {
    /// value: 4
    public static let tiny = COFSpacing.tiny
    /// value: 8
    public static let small = COFSpacing.small
    /// value: 12
    public static let xSmall = COFSpacing.xSmall
    /// value: 16
    public static let medium = COFSpacing.medium
    /// value: 24
    public static let xMedium = COFSpacing.xMedium
    /// value: 32
    public static let large = COFSpacing.large
    /// value: 48
    public static let xLarge = COFSpacing.xLarge
    /// value: 64
    public static let xxLarge = COFSpacing.xxLarge
}

extension UIEdgeInsets {
    /// value: 4
    public static let tiny = UIEdgeInsets(top: .tiny, left: .tiny, bottom: .tiny, right: .tiny)
    /// value: 8
    public static let small = UIEdgeInsets(top: .small, left: .small, bottom: .small, right: .small)
    /// value: 16
    public static let medium = UIEdgeInsets(top: .medium, left: .medium, bottom: .medium, right: .medium)
    /// value: 24
    public static let xMedium = UIEdgeInsets(top: .xMedium, left: .xMedium, bottom: .xMedium, right: .xMedium)
    /// value: 32
    public static let large = UIEdgeInsets(top: .large, left: .large, bottom: .large, right: .large)
    
    /// only vertical value: 4
    public static let vTiny = UIEdgeInsets(top: .tiny, left: 0, bottom: .tiny, right: 0)
    /// only vertical value: 8
    public static let vSmall = UIEdgeInsets(top: .small, left: 0, bottom: .small, right: 0)
    /// only vertical value: 16
    public static let vMedium = UIEdgeInsets(top: .medium, left: 0, bottom: .medium, right: 0)
    /// only vertical value: 24
    public static let vxMedium = UIEdgeInsets(top: .xMedium, left: 0, bottom: .xMedium, right: 0)
    /// only vertical value: 32
    public static let vLarge = UIEdgeInsets(top: .large, left: 0, bottom: .large, right: 0)
    
    /// only horizontal value: 4
    public static let hTiny = UIEdgeInsets(top: 0, left: .tiny, bottom: 0, right: .tiny)
    /// only horizontal value: 8
    public static let hSmall = UIEdgeInsets(top: 0, left: .small, bottom: 0, right: .small)
    /// only horizontal value: 16
    public static let hMedium = UIEdgeInsets(top: 0, left: .medium, bottom: 0, right: .medium)
    /// only horizontal value: 24
    public static let hxMedium = UIEdgeInsets(top: 0, left: .xMedium, bottom: 0, right: .xMedium)
    /// only horizontal value: 32
    public static let hLarge = UIEdgeInsets(top: 0, left: .large, bottom: 0, right: .large)
}

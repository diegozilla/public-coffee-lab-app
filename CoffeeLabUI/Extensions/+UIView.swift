//
//  +UITableView.swift
//  CoffeeLabUI
//
//  Created by Diego Espinoza on 9/12/22.
//

import UIKit

extension UIView {
    public func attachTop(rootView: UIView, usingSafeArea: Bool = true, constant: CGFloat = .zero) -> NSLayoutConstraint {
        let constraint = usingSafeArea ? rootView.safeAreaLayoutGuide.topAnchor : rootView.topAnchor
        return self.topAnchor.constraint(equalTo: constraint,
                                         constant: constant)
    }
    
    public func attachLeading(rootView: UIView, usingSafeArea: Bool = true, constant: CGFloat = .zero) -> NSLayoutConstraint {
        let constraint = usingSafeArea ? rootView.safeAreaLayoutGuide.leadingAnchor : rootView.leadingAnchor
        return self.leadingAnchor.constraint(equalTo: constraint,
                                             constant: constant)
    }
    
    public func attachBottom(rootView: UIView, usingSafeArea: Bool = true, constant: CGFloat = .zero) -> NSLayoutConstraint {
        let constraint = usingSafeArea ? rootView.safeAreaLayoutGuide.bottomAnchor : rootView.bottomAnchor
        return self.bottomAnchor.constraint(equalTo: constraint,
                                            constant: constant)
    }
    
    public func attachTrailing(rootView: UIView, usingSafeArea: Bool = true, constant: CGFloat = .zero) -> NSLayoutConstraint {
        let constraint = usingSafeArea ? rootView.safeAreaLayoutGuide.trailingAnchor : rootView.trailingAnchor
        return self.trailingAnchor.constraint(equalTo: constraint, constant: constant)
    }
    
    public func attachToSafeArea(rootView: UIView, margin: UIEdgeInsets = .zero) {
        rootView.addSubview(self)

        NSLayoutConstraint.activate([
            self.topAnchor.constraint(equalTo: rootView.safeAreaLayoutGuide.topAnchor, constant: margin.top),
            self.leadingAnchor.constraint(equalTo: rootView.safeAreaLayoutGuide.leadingAnchor, constant: margin.left),
            self.trailingAnchor.constraint(equalTo: rootView.safeAreaLayoutGuide.trailingAnchor, constant: -margin.right),
            self.bottomAnchor.constraint(equalTo: rootView.safeAreaLayoutGuide.bottomAnchor, constant: -margin.bottom)
        ])
    }

    public func attachTo(rootView: UIView, margin: UIEdgeInsets = .zero) {
        rootView.addSubview(self)

        NSLayoutConstraint.activate([
            self.topAnchor.constraint(equalTo: rootView.topAnchor, constant: margin.top),
            self.leadingAnchor.constraint(equalTo: rootView.leadingAnchor, constant: margin.left),
            self.trailingAnchor.constraint(equalTo: rootView.trailingAnchor, constant: -margin.right),
            self.bottomAnchor.constraint(equalTo: rootView.bottomAnchor, constant: -margin.bottom)
        ])
    }

    public func attachVerticalAndLeadingTo(rootView: UIView, margin: UIEdgeInsets = .zero) {
        rootView.addSubview(self)

        NSLayoutConstraint.activate([
            self.topAnchor.constraint(equalTo: rootView.topAnchor, constant: margin.top),
            self.leadingAnchor.constraint(equalTo: rootView.leadingAnchor, constant: margin.left),
            self.bottomAnchor.constraint(equalTo: rootView.bottomAnchor, constant: -margin.bottom)
        ])
    }
}

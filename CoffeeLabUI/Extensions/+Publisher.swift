//
//  +Publisher.swift
//  CoffeeLabUI
//
//  Created by Diego Espinoza on 22/12/22.
//

import Foundation
import Combine

extension Publisher where Output: Equatable {
    public func filterIfEqual(to output: Output) -> AnyPublisher<Output, Failure> {
        self.filter { currentOutput in
            currentOutput != output
        }
        .eraseToAnyPublisher()
    }
}


//
//  +UIViewController.swift
//  CoffeeLabUI
//
//  Created by Diego Espinoza on 9/12/22.
//

import UIKit

extension UIViewController {
    public func addContainer(viewController: UIViewController) {
        let childView = viewController.view!
        childView.translatesAutoresizingMaskIntoConstraints = false
        addChild(viewController)
        childView.attachToSafeArea(rootView: self.view, margin: .zero)
        viewController.didMove(toParent: self)
    }

    public func setTitle(_ title: String) {
        self.title = title
    }
    
    public func setTabBarItem(title: String, icon: UIImage?, selectedIcon: UIImage?) {
        self.tabBarItem = UITabBarItem(title: title, image: icon, selectedImage: selectedIcon)
    }
    
    public func setNeverLargeTitle() {
        navigationItem.largeTitleDisplayMode = .never
    }
    
    public func setLargeTitle() {
        navigationItem.largeTitleDisplayMode = .always
    }
}

extension UINavigationController {
    public func setLargeTitleNavigation() {
        navigationBar.prefersLargeTitles = true
        navigationItem.largeTitleDisplayMode = .automatic
    }
}

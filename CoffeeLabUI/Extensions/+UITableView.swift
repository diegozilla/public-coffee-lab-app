//
//  +UITableView.swift
//  CoffeeLabUI
//
//  Created by Diego Espinoza on 9/12/22.
//

import UIKit

extension UITableViewCell {
    public static var reuseIdentifier: String {
        String(describing: self)
    }

    public var reuseIdentifier: String {
        type(of: self).reuseIdentifier
    }
}

extension UITableView {
    public func register<T: UITableViewCell>(_ type: T.Type) {
        register(T.self, forCellReuseIdentifier: T.reuseIdentifier)
    }

    public func reuse<T: UITableViewCell>(_ type: T.Type, _ indexPath: IndexPath) -> T {
        dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as! T
    }
}

//
//  +UIFont.swift
//  CoffeeLabUI
//
//  Created by Diego Espinoza on 10/12/22.
//

import UIKit

extension UIFont {
    public static func preferredFont(for style: TextStyle, weight: Weight) -> UIFont {
        let metrics = UIFontMetrics(forTextStyle: style)
        let desc = UIFontDescriptor.preferredFontDescriptor(withTextStyle: style)
        let font = UIFont.systemFont(ofSize: desc.pointSize, weight: weight)
        return metrics.scaledFont(for: font)
    }
}

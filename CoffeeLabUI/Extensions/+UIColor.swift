//
//  +UIColor.swift
//  CoffeeLabUI
//
//  Created by Diego Espinoza on 29/01/23.
//

import UIKit

extension UIColor {
    public static let theme = AppTheme()
    
    public struct AppTheme {
        /// UIColor.systemCyan
        public let primaryColor = UIColor.systemCyan
        /// UIColor.systemRed
        public let dangerColor = UIColor.systemRed
        /// UIColor.systemBrown
        public let accentColor = UIColor.systemBrown
        /// UIColor.systemOrange
        public let accent2Color = UIColor.systemOrange
    }
}

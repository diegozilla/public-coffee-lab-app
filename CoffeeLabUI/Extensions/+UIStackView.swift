//
//  +UIStackView.swift
//  CoffeeLabUI
//
//  Created by Diego Espinoza on 11/12/22.
//

import UIKit

extension UIStackView {
    public static func createHStackView(spacing: CGFloat = .medium) -> UIStackView {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.spacing = spacing
        stackView.distribution = .fill
        return stackView
    }

    public static func createVStackView(spacing: CGFloat = .medium) -> UIStackView {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.spacing = spacing
        stackView.distribution = .fill
        return stackView
    }
}

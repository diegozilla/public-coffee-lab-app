//
//  COFScrollVStackConfiguration.swift
//  CoffeeLabUI
//
//  Created by Diego Espinoza on 9/12/22.
//

import UIKit

public struct COFScrollStackViewConfiguration {
    let axis: NSLayoutConstraint.Axis
    let spacing: CGFloat
    let alignment: UIStackView.Alignment
    let distribution: UIStackView.Distribution
    let margin: UIEdgeInsets
    
    public static var defaultVertical = COFScrollStackViewConfiguration(
        axis: .vertical,
        spacing: .small,
        alignment: .leading,
        distribution: .fill,
        margin: .medium
    )
    
    public static var mediumVertical = COFScrollStackViewConfiguration(
        axis: .vertical,
        spacing: .medium,
        alignment: .leading,
        distribution: .fill,
        margin: .medium
    )

    public static var defaultHorizontal = COFScrollStackViewConfiguration(
        axis: .horizontal,
        spacing: .small,
        alignment: .top,
        distribution: .fill,
        margin: .vMedium
    )
}

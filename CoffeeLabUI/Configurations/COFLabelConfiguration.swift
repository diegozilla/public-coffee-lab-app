//
//  COFLabelConfiguration.swift
//  CoffeeLabUI
//
//  Created by Diego Espinoza on 13/12/22.
//

import UIKit

public struct COFLabelConfiguration {
    public let font: UIFont
    public let color: UIColor

    public static var title = COFLabelConfiguration(font: .preferredFont(for: .title1, weight: .bold),
                                                    color: .label)
    
    public static var title2 = COFLabelConfiguration(font: .preferredFont(for: .headline, weight: .bold),
                                                        color: .label)
    
    public static var body = COFLabelConfiguration(font: .preferredFont(forTextStyle: .body),
                                                   color: .label)

    public static var bodyDetail = COFLabelConfiguration(font: .preferredFont(for: .callout, weight: .light),
                                                         color: .secondaryLabel)
    
    public static var inputTitle = COFLabelConfiguration(font: .preferredFont(for: .footnote, weight: .medium),
                                                         color: .secondaryLabel)
    
    public static var inputContent = COFLabelConfiguration(font: .preferredFont(for: .body, weight: .regular),
                                                           color: .label)
    
    public static var inputPlaceholder = COFLabelConfiguration(font: .preferredFont(for: .body, weight: .regular),
                                                               color: .tertiaryLabel)
    
    public static var error = COFLabelConfiguration(font: .preferredFont(for: .subheadline, weight: .regular),
                                                    color: .systemGray6)
}

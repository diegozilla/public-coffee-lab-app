//
//  COFButton.swift
//  CoffeeLabUI
//
//  Created by Diego Espinoza on 4/01/23.
//

import UIKit
import Combine

open class COFButton: UIButton {
    private var _onAction: PassthroughSubject<Void, Never> = .init()
    
    public var onAction: AnyPublisher<Void, Never> { _onAction.eraseToAnyPublisher() }

    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }

    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        addTarget(self, action: #selector(onAction(sender:)), for: .touchUpInside)
    }

    @objc func onAction(sender: COFButton) {
        _onAction.send(())
    }
}

extension COFButton {
    public static func filled(title: String) -> COFButton {
        let button = COFButton(type: .system)
        let attributes = AttributeContainer([.font : UIFont.preferredFont(for: .body, weight: .bold)])
        var configuration = UIButton.Configuration.filled()
        configuration.attributedTitle = AttributedString(title, attributes: attributes)
        configuration.contentInsets = .init(top: .medium,
                                            leading: 0,
                                            bottom: .medium,
                                            trailing: 0)
        button.configuration = configuration
        return button
    }
}

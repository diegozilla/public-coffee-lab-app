//
//  COFLabel.swift
//  CoffeeLabUI
//
//  Created by Diego Espinoza on 10/12/22.
//

import UIKit

open class COFLabel: UILabel {
    public init(text: String? = nil,
                accessibilityLabel: String? = nil,
                configuration: COFLabelConfiguration) {
        super.init(frame: .zero)
        self.textColor = configuration.color
        self.font = configuration.font
        self.accessibilityTraits = .staticText
        self.adjustsFontForContentSizeCategory = true
        self.set(text: text, accessibilityLabel: accessibilityLabel)
    }

    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public func set(text: String?, accessibilityLabel: String? = nil) {
        self.text = text
        self.accessibilityLabel = accessibilityLabel != nil ? accessibilityLabel : text
    }
}


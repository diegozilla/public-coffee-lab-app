//
//  COFView.swift
//  CoffeeLabUI
//
//  Created by Diego Espinoza on 13/12/22.
//

import UIKit
import Combine

open class COFView: UIView {
    public var error: Error? {
        didSet { onError.send(error) }
    }
    
    private lazy var errorBanner: COFErrorBanner = {
        let banner = COFErrorBanner()
        banner.translatesAutoresizingMaskIntoConstraints = false
        banner.isHidden = true
        return banner
    }()

    private var onError: PassthroughSubject<Error?, Never> = .init()
    private var isPresentingError = false
    private var cancellables = Set<AnyCancellable>()
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        setupErrorBinding()
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        addSubview(errorBanner)
        NSLayoutConstraint.activate([
            errorBanner.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: .medium),
            errorBanner.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: .medium),
            errorBanner.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -.medium),
        ])
    }
    
    private func setupErrorBinding() {
        onError
            .filter { [weak self] error in
                guard let owner = self else { return false }
                return !owner.isPresentingError && error != nil
            }
            .receive(on: DispatchQueue.main)
            .handleEvents(receiveOutput: { [weak self] error in
                self?.presentErrorBanner(usingError: error)
            })
            .delay(for: .seconds(2), scheduler: DispatchQueue.main)
            .sink { [weak self] _ in
                self?.error = nil
                self?.hideErrorBanner()
            }
            .store(in: &cancellables)
    }
    
    public func moveErrorBannerToTop() {
        bringSubviewToFront(errorBanner)
    }
    
    private func presentErrorBanner(usingError error: Error?) {
        isPresentingError = true
        errorBanner.alpha = 0
        errorBanner.isHidden = false
        errorBanner.transform = CGAffineTransform(translationX: 0, y: -self.errorBanner.bounds.height)
        errorBanner.title = error?.localizedDescription
        
        UIView.animate(withDuration: 0.5,
                       delay: 0,
                       usingSpringWithDamping: 0.9,
                       initialSpringVelocity: 0.8) {
            self.errorBanner.alpha = 1
            self.errorBanner.transform = .identity
        }
    }
    
    private func hideErrorBanner() {
        isPresentingError = false
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.8) {
            self.errorBanner.alpha = 0
            self.errorBanner.transform = CGAffineTransform(translationX: 0, y: -self.errorBanner.bounds.height)
        } completion: { _ in
            self.errorBanner.isHidden = true
        }

    }
}

//
//  COFContainerView.swift
//  CoffeeLabUI
//
//  Created by Diego Espinoza on 26/01/23.
//

import UIKit

open class COFContainerStackView: UIView {
    public let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        return stackView
    }()
    
    public init(axis: NSLayoutConstraint.Axis, spacing: CGFloat = .small) {
        super.init(frame: .zero)
        self.stackView.axis = axis
        self.stackView.spacing = spacing
        setupView()
    }

    public required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        addSubview(stackView)
        stackView.attachTo(rootView: self)
    }
}

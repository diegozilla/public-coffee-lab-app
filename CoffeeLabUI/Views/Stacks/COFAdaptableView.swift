//
//  COFAdaptableView.swift
//  CoffeeLabUI
//
//  Created by Diego Espinoza on 10/12/22.
//

import UIKit

open class COFAdaptableView: UIView {
    lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
  
    public override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .tertiarySystemGroupedBackground
        addSubview(stackView)
        stackView.attachToSafeArea(rootView: self, margin: .zero)
        configureView(for: self.traitCollection)
    }
  
  required public init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
  }
  
  open override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
      super.traitCollectionDidChange(previousTraitCollection)
      if previousTraitCollection?.preferredContentSizeCategory != traitCollection.preferredContentSizeCategory {
          configureView(for: traitCollection)
      }
  }
  
  private func configureView(for traitCollection: UITraitCollection) {
      let contentSize = traitCollection.preferredContentSizeCategory
      if contentSize.isAccessibilityCategory {
          stackView.axis = .vertical
          stackView.alignment = .leading
          stackView.spacing = 0
      } else {
          stackView.axis = .horizontal
          stackView.alignment = .center
          stackView.spacing = .small
      }
  }
}


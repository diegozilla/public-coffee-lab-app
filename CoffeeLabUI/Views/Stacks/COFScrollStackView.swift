//
//  CLAScrollVerticalStack.swift
//  CoffeeLabUI
//
//  Created by Diego Espinoza on 8/12/22.
//

import UIKit
import Combine

open class COFScrollStackView: UIView {
    public var contentViews: [UIView] {
        stackView.arrangedSubviews
    }

    public lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        return stackView
    }()
    
    public lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()

    private let configuration: COFScrollStackViewConfiguration
    private let handleKeyboard: Bool
    private var bottomConstraint: NSLayoutConstraint?
    private var cancellables = Set<AnyCancellable>()

    public init(configuration: COFScrollStackViewConfiguration = .defaultVertical,
                handleKeyboard: Bool = false) {
        self.configuration = configuration
        self.handleKeyboard = handleKeyboard
        super.init(frame: .zero)
        setupView()
        if handleKeyboard {
            setupKeyboardBindings()
        }
    }

    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupView() {
        let margin = configuration.margin

        stackView.spacing = configuration.spacing
        stackView.distribution = configuration.distribution
        stackView.alignment = configuration.alignment
        stackView.axis = configuration.axis
        
        addSubview(scrollView)
        scrollView.addSubview(stackView)
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor),
            stackView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: margin.top),
            stackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: margin.left),
            stackView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: -margin.right),
            stackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: -margin.bottom)
        ])

        switch stackView.axis {
        case .horizontal:
            let constant = margin.top + margin.bottom
            stackView.heightAnchor.constraint(equalTo: scrollView.heightAnchor, constant: -constant).isActive = true
        case .vertical:
            let constant = margin.left + margin.right
            stackView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -constant).isActive = true
        default:
            fatalError("This axis is not supported")
        }
    }

    private func setupKeyboardBindings() {
        NotificationCenter.default.publisher(for: UIResponder.keyboardWillShowNotification)
            .compactMap { notification -> CGRect? in
                notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect
            }
            .sink { [weak self] rect in
                self?.setContentInset(UIEdgeInsets(top: 0,
                                                   left: 0,
                                                   bottom: rect.height,
                                                   right: 0))
            }
            .store(in: &cancellables)

        NotificationCenter.default.publisher(for: UIResponder.keyboardWillHideNotification)
            .sink { [weak self] _ in
                self?.setContentInset(.zero)
            }
            .store(in: &cancellables)
    }

    public func addArrangeViews(_ views: [UIView]) {
        views.forEach { stackView.addArrangedSubview($0) }
    }
    
    public func setSubviewsEqualWidthToStackView() {
        stackView.arrangedSubviews.forEach { view in
            view.widthAnchor.constraint(equalTo: stackView.widthAnchor).isActive = true
        }
    }
    
    public func setSubviewsEqualHeightToStackView() {
        stackView.arrangedSubviews.forEach { view in
            view.heightAnchor.constraint(equalTo: stackView.heightAnchor).isActive = true
        }
    }

    public func setSpacing(_ spacing: CGFloat, after view: UIView) {
        stackView.setCustomSpacing(spacing, after: view)
    }

    public func setContentInset(_ inset: UIEdgeInsets) {
        scrollView.contentInset = inset
    }
}

//
//  COFErrorBanner.swift
//  CoffeeLabUI
//
//  Created by Diego Espinoza on 13/12/22.
//

import UIKit

internal class COFErrorBanner: UIView {
    var title: String? {
        get { titleLabel.text }
        set { titleLabel.text = newValue }
    }
    
    private lazy var titleLabel: COFLabel = {
        let label = COFLabel(configuration: .error)
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = .systemRed
        layer.cornerRadius = .medium
        layer.masksToBounds = true
        addSubview(titleLabel)
        titleLabel.attachTo(rootView: self,
                            margin: UIEdgeInsets(top: .xMedium, left: .medium, bottom: .xMedium, right: .medium))
    }
}

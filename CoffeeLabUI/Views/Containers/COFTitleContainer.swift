//
//  COFLargeTitleContainer.swift
//  CoffeeLabUI
//
//  Created by Diego Espinoza on 10/12/22.
//

import UIKit

public class COFTitleContainer: UIViewController {
    public init(title: String, viewController: UIViewController) {
        super.init(nibName: nil, bundle: nil)
        self.title = title
        self.addContainer(viewController: viewController)
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemGroupedBackground
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

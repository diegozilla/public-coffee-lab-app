//
//  COFNavigationController.swift
//  CoffeeLabUI
//
//  Created by Diego Espinoza on 9/12/22.
//

import UIKit

open class COFNavigationController: UINavigationController {
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        navigationBar.prefersLargeTitles = true
        navigationItem.largeTitleDisplayMode = .automatic
    }
}


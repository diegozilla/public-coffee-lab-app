//
//  COFTabBarItemContainer.swift
//  CoffeeLabUI
//
//  Created by Diego Espinoza on 10/12/22.
//

import UIKit

public class COFTabBarItemContainer: UIViewController {
    public init(title: String, icon: UIImage, selectedIcon: UIImage, viewController: UIViewController) {
        super.init(nibName: nil, bundle: nil)
        tabBarItem = UITabBarItem(title: title, image: icon, selectedImage: selectedIcon)
        self.addContainer(viewController: viewController)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

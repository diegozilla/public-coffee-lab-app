//
//  COFTextFieldView.swift
//  CoffeeLabUI
//
//  Created by Diego Espinoza on 17/12/22.
//

import UIKit
import Combine

public class COFTextFieldView: UIView, UITextFieldDelegate {
    private enum TitleStyle {
        case hidden
        case presented
    }

    private var presentTextAnimator = UIViewPropertyAnimator(duration: 0.25, curve: .easeInOut, animations: nil)
    private var dismissTextAnimator = UIViewPropertyAnimator(duration: 0.25, curve: .easeInOut, animations: nil)

    private var setup: TitleStyle = .hidden
    private var isEditing = false
    private var _onTextChange: PassthroughSubject<String?, Never> = .init()

    public var onTextChange: AnyPublisher<String?, Never> { _onTextChange.share().eraseToAnyPublisher() }

    public var text: String? {
        get { return textField.text }
        set {
            guard textField.text != newValue else { return }
            textField.text = newValue
            setup = newValue == nil ? .hidden : .presented
            updateInnerLayout(isLayoutNeeded: false)
        }
    }

    public var placeholderText: String? {
        get { return textField.placeholder }
        set { textField.attributedPlaceholder = NSAttributedString(string: newValue ?? "",
                                                                   attributes: [.font : UIFont.preferredFont(for: .body,
                                                                                                             weight: .regular)]) }
    }

    public var font: UIFont? {
        get { return textField.font }
        set { textField.font = newValue }
    }

    public var textColor: UIColor? {
        get { return textField.textColor }
        set { textField.textColor = newValue }
    }

    public var title: String? {
        get { return titleLabel.text }
        set { titleLabel.text = newValue }
    }

    private var textCenterConstraint: NSLayoutConstraint!
    private var textTopConstraint: NSLayoutConstraint!
    private var textHeightConstraint: NSLayoutConstraint!

    private lazy var textField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.addTarget(self, action: #selector(onTextChange(sender:)), for: .editingChanged)
        tf.adjustsFontForContentSizeCategory = true
        tf.font = UIFont.preferredFont(for: .body, weight: .regular)
        return tf
    }()

    private lazy var titleLabel: UILabel = {
        let l = UILabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.textColor = UIColor.secondaryLabel
        l.isHidden = true
        l.font = UIFont.preferredFont(for: .subheadline, weight: .medium)
        l.alpha = 0
        l.numberOfLines = 2
        l.adjustsFontForContentSizeCategory = true
        return l
    }()
    
    public init(title: String, placeholder: String) {
        super.init(frame: .zero)
        self.title = title
        self.placeholderText = placeholder
        setupView()
    }

    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupView() {
        layer.cornerRadius = 8
        layer.borderWidth = 2
        setFocus(false)
        textField.delegate = self
        addSubview(textField)
        addSubview(titleLabel)
        setupLayout()
    }

    private func setupLayout() {
        textCenterConstraint = textField.centerYAnchor.constraint(equalTo: centerYAnchor)
        textTopConstraint = textField.topAnchor.constraint(equalTo: titleLabel.bottomAnchor)

        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: .xSmall),
            titleLabel.leadingAnchor.constraint(equalTo: textField.leadingAnchor),
            titleLabel.trailingAnchor.constraint(equalTo: textField.trailingAnchor),
            textCenterConstraint,
            textField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: .medium),
            textField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -.medium),
            textField.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -.xSmall),
            heightAnchor.constraint(greaterThanOrEqualToConstant: .xxLarge)
        ])
    }

    public func textFieldDidBeginEditing(_ textField: UITextField) {
        isEditing = true
        setFocus(true)
    }

    public func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        isEditing = false
        setFocus(false)
    }
    
    public func bind(to subject: CurrentValueSubject<String?, Never>,
                     storeIn cancellables: inout Set<AnyCancellable>) {
        subject.sink { [weak self] text in
            if text != self?.text { self?.text = text }
        }
        .store(in: &cancellables)

        onTextChange.sink { text in
            if text != subject.value { subject.send(text) }
        }
        .store(in: &cancellables)
    }
    
    private func setFocus(_ isFocused: Bool) {
        if isFocused {
            layer.borderColor = UIColor.theme.primaryColor.cgColor
            titleLabel.textColor = UIColor.theme.primaryColor
        } else {
            layer.borderColor = UIColor.tertiarySystemGroupedBackground.cgColor
            titleLabel.textColor = UIColor.secondaryLabel
        }
    }

    @objc private func onTextChange(sender: UITextField) {
        if let text = self.text, !text.isEmpty {
            if setup == .hidden {
                setup = .presented
                updateInnerLayout()
            }
        } else {
            if setup == .presented {
                setup = .hidden
                updateInnerLayout()
            }
        }
        _onTextChange.send(sender.text)
    }

    private func setPresenterAnimator() {
        presentTextAnimator.addAnimations {
            self.titleLabel.alpha = 1
            self.layoutIfNeeded()
        }
    }

    private func setDismissAnimator() {
        dismissTextAnimator.addAnimations {
            self.titleLabel.alpha = 0
            self.layoutIfNeeded()
        }

        dismissTextAnimator.addCompletion { _ in
            self.titleLabel.isHidden = true
        }
    }

    private func setConstraintForSingleTextField(_ value: Bool) {
        textTopConstraint.isActive = !value
        textCenterConstraint.isActive = value
    }

    private func updateInnerLayout(isLayoutNeeded: Bool = true) {
        if isEditing {
            switch setup {
            case .hidden:
                setDismissAnimator()
                setConstraintForSingleTextField(true)

                if presentTextAnimator.isRunning { presentTextAnimator.stopAnimation(true) }
                dismissTextAnimator.startAnimation()
            case .presented:
                setPresenterAnimator()
                setConstraintForSingleTextField(false)
                titleLabel.isHidden = false

                if dismissTextAnimator.isRunning { dismissTextAnimator.stopAnimation(true) }
                presentTextAnimator.startAnimation()
            }
        } else {
            switch setup {
            case .hidden:
                titleLabel.isHidden = true
                titleLabel.alpha = 0
                setConstraintForSingleTextField(true)
            case .presented:
                titleLabel.isHidden = false
                titleLabel.alpha = 1
                setConstraintForSingleTextField(false)
            }

            if isLayoutNeeded { layoutIfNeeded() }
        }
    }
}

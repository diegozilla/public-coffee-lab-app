//
//  COFInputView.swift
//  CoffeeLabUI
//
//  Created by Diego Espinoza on 11/12/22.
//

import UIKit

open class COFInputView: UIView {
    open var showSeparator: Bool { false }

    public var title: String? {
        get { titleLabel.text }
        set { titleLabel.text = newValue }
    }
    
    public lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = .tiny
        view.layer.masksToBounds = false
        return view
    }()
    
    private lazy var vStackView: UIStackView = {
        let stackView = UIStackView.createVStackView(spacing: .tiny)
        stackView.distribution = .fill
        return stackView
    }()
    
    private lazy var titleLabel: COFLabel = {
        let label = COFLabel(configuration: .inputTitle)
        return label
    }()

    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    public required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open func setupView() {
        containerView.attachTo(rootView: self)
        vStackView.attachToSafeArea(rootView: containerView,
                                    margin: UIEdgeInsets(top: .small, left: .zero, bottom: .small, right: .zero))
        vStackView.addArrangedSubview(titleLabel)
        layer.cornerRadius = .small
    }
    
    public func addArrange(subview: UIView) {
        vStackView.addArrangedSubview(subview)
    }

    open override func draw(_ rect: CGRect) {
        if showSeparator {
            let path = UIBezierPath()
            path.move(to: .init(x: 0, y: rect.height - 1))
            path.addLine(to: .init(x: rect.width, y: rect.height - 1))
            path.lineWidth = 1
            UIColor.systemGray3.setStroke()
            path.stroke()
            path.close()
        } else {
            super.draw(rect)
        }
    }
}

open class COFInputSectionView: UIView {
    private lazy var vStackView: UIStackView = {
        let stackView = UIStackView.createVStackView(spacing: .small)
        return stackView
    }()
    
    public init(views: [UIView]) {
        super.init(frame: .zero)
        views.forEach { vStackView.addArrangedSubview($0) }
        setupView()
    }
    
    public required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupView() {
        layer.cornerRadius = .small
        layer.masksToBounds = true
        vStackView.attachTo(rootView: self,
                            margin: UIEdgeInsets(top: .small, left: .medium, bottom: .small, right: .medium))
    }
}

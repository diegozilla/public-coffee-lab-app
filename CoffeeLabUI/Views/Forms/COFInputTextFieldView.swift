//
//  COFInputTextFieldView.swift
//  CoffeeLabUI
//
//  Created by Diego Espinoza on 11/12/22.
//

import UIKit
import Combine

open class COFInputTextFieldView: COFInputView {
    open override var showSeparator: Bool { true }
    public var onTextUpdate: AnyPublisher<String?, Never> {
        _onTextUpdate.eraseToAnyPublisher()
    }
    public var onReturn: AnyPublisher<UIReturnKeyType, Never> {
        _onReturn.eraseToAnyPublisher()
    }
    
    public var placeholder: String? {
        get { textField.placeholder }
        set { textField.placeholder = newValue }
    }
    
    public var text: String? {
        get { textField.text }
        set { textField.text = newValue }
    }
    
    public var returnKey: UIReturnKeyType {
        get { textField.returnKeyType }
        set { textField.returnKeyType = newValue }
    }
    
    private var _onTextUpdate: PassthroughSubject<String?, Never> = .init()
    private var _onReturn: PassthroughSubject<UIReturnKeyType, Never> = .init()
    
    private lazy var textField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.layer.cornerRadius = .tiny
        textField.heightAnchor.constraint(equalToConstant: 30).isActive = true
        textField.delegate = self
        return textField
    }()
    
    public init(title: String, text: String?, placeholder: String?) {
        super.init(frame: .zero)
        self.title = title
        textField.text = text
        textField.placeholder = placeholder
    }

    public required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func setAsFirstResponse(_ isFirstResponder: Bool) {
        if isFirstResponder {
            textField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
    }

    open override func setupView() {
        super.setupView()
        backgroundColor = .systemBackground
        addArrange(subview: textField)
        textField.addTarget(self, action: #selector(onTextChange(sender:)), for: .editingChanged)
    }
    
    @objc private func onTextChange(sender: UITextField) {
        _onTextUpdate.send(sender.text)
    }
}

extension COFInputTextFieldView: UITextFieldDelegate {
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        _onReturn.send(textField.returnKeyType)
        return true
    }
}

//
//  COFInputImageView.swift
//  CoffeeLabUI
//
//  Created by Diego Espinoza on 12/12/22.
//

import UIKit
import Combine

open class COFInputImageView: UIControl {
    
    public var onAction: AnyPublisher<Void, Never> { _onAction.eraseToAnyPublisher() }

    public var title: String? {
        get { titleLabel.text }
        set { titleLabel.text = newValue }
    }
    
    public var placeholder: String? {
        get { placeholderLabel.text }
        set { placeholderLabel.text = newValue }
    }
    
    public var selectedImage: Data? {
        get { imageView.image?.jpegData(compressionQuality: 1) }
        set {
            if let data = newValue {
                imageView.image = UIImage(data: data)
                showImage()
            } else {
                imageView.image = nil
                showPlaceholder()
            }
        }
    }

    private lazy var titleLabel: COFLabel = {
        let label = COFLabel(configuration: .inputTitle)
        label.isUserInteractionEnabled = false
        return label
    }()
    
    private lazy var placeholderLabel: COFLabel = {
        let label = COFLabel(configuration: .inputPlaceholder)
        label.isUserInteractionEnabled = false
        return label
    }()
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.isHidden = true
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = .small
        imageView.layer.masksToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        return imageView
    }()
    
    private lazy var iconImageView: UIImageView = {
        let image = UIImage(systemName: "chevron.right")
        let imageView = UIImageView(image: image)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .center
        imageView.setContentHuggingPriority(.required, for: .horizontal)
        return imageView
    }()
    
    private lazy var hStackView: UIStackView = {
        let stackView = UIStackView.createHStackView(spacing: .tiny)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        stackView.isUserInteractionEnabled = false
        return stackView
    }()
    
    private var _onAction: PassthroughSubject<Void, Never> = .init()
    
    public override init(frame: CGRect) {
        super.init(frame: .zero)
        setupView()
    }
    
    public required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        hStackView.attachVerticalAndLeadingTo(rootView: self)
        addSubview(iconImageView)
        hStackView.addArrangedSubview(titleLabel)
        hStackView.addArrangedSubview(placeholderLabel)
        hStackView.addArrangedSubview(imageView)

        NSLayoutConstraint.activate([
            iconImageView.trailingAnchor.constraint(equalTo: trailingAnchor),
            iconImageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            iconImageView.leadingAnchor.constraint(equalTo: hStackView.trailingAnchor, constant: .small),
            iconImageView.heightAnchor.constraint(equalToConstant: 30),
            iconImageView.widthAnchor.constraint(equalToConstant: 30)
        ])
    
        addTarget(self, action: #selector(onTap), for: .touchUpInside)
    }

    @objc private func onTap() {
        _onAction.send(())
    }
    
    // TODO: - add animations
    
    private func showImage() {
        placeholderLabel.isHidden = true
        imageView.isHidden = false
    }
    
    private func showPlaceholder() {
        placeholderLabel.isHidden = false
        imageView.isHidden = true
    }
}

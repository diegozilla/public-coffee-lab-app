//
//  COFMyRecipesViewController.swift
//  CoffeeLabFlow
//
//  Created by Diego Espinoza on 13/12/22.
//

import UIKit
import CoffeeLabUI

final class COFMyRecipesViewController: UIViewController {
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        let label = COFLabel(configuration: .subtitle)
//        label.set(text: "Recipe list will go here")
//        label.textAlignment = .center
//        label.translatesAutoresizingMaskIntoConstraints = false
//        view.addSubview(label)
//        NSLayoutConstraint.activate([
//            label.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
//            label.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
//            label.centerYAnchor.constraint(equalTo: view.centerYAnchor)
//        ])
//    }
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        let l = UILabel()
        
        l.text = "What's the coffee name?"
        l.translatesAutoresizingMaskIntoConstraints = false
        l.numberOfLines = 2
        l.font = UIFont.preferredFont(for: .largeTitle, weight: .bold)
        
        let tf = COFTextFieldView()
        tf.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(l)
        view.addSubview(tf)
        NSLayoutConstraint.activate([
            l.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 16),
            l.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16),
            l.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16),
            tf.topAnchor.constraint(equalTo: l.bottomAnchor, constant: 16),
            tf.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16),
            tf.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16),
            tf.heightAnchor.constraint(greaterThanOrEqualToConstant: 65)
        ])
        
        tf.title = "Name"
        tf.placeholderText = "Enter coffee name"
    }
}

//
//  COFCreateCoffeePresenter.swift
//  CoffeeLabFlow
//
//  Created by Diego Espinoza on 11/12/22.
//

import Foundation
import Combine

final class COFCreateCoffeePresenter {
    @Published var name: String?
    @Published var roastType: String?
    @Published var origin: String?
    @Published var description: String?
    @Published var variety: String?

    @Published var photo: Data?
    
    @Published var error: Error?
    @Published var isLoading = false
    
    var isUpdate: Bool
    
    var actionTitle: String {
        isUpdate ? "Update" : "Create"
    }

    init(isUpdate: Bool) {
        self.isUpdate = isUpdate
    }
}

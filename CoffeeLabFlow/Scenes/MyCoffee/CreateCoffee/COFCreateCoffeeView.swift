//
//  COFCreateCoffeeView.swift
//  CoffeeLabFlow
//
//  Created by Diego Espinoza on 11/12/22.
//

import UIKit
import CoffeeLabUI
import Combine

final class COFCreateCoffeeView: COFView {
    
    // MARK: - Bindings
    
    var nameTextChange: AnyPublisher<String?, Never> { nameTextField.onTextUpdate }
    var varietyTextChange: AnyPublisher<String?, Never> { varietyTextField.onTextUpdate }
    var roastTextChange: AnyPublisher<String?, Never> { roastTextField.onTextUpdate }
    var originTextChange: AnyPublisher<String?, Never> { originTextField.onTextUpdate }
    var descriptionTextChange: AnyPublisher<String?, Never> { descriptionTextField.onTextUpdate }
    var imageAction: AnyPublisher<Void, Never> { imageInputView.onAction }
    
    var onSaveAction: AnyPublisher<Void, Never> { _onSaveAction.eraseToAnyPublisher() }
    
    private let _onSaveAction: PassthroughSubject<Void, Never> = .init()

    // MARK: - Subviews
    
    private lazy var scrollView: COFScrollStackView = {
        let scrollView = COFScrollStackView(configuration: .mediumVertical)
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
    
    internal lazy var nameTextField: COFInputTextFieldView = { createName() }()
    internal lazy var varietyTextField: COFInputTextFieldView = { createVariety() }()
    internal lazy var originTextField: COFInputTextFieldView = { createOrigin() }()
    internal lazy var roastTextField: COFInputTextFieldView = { createRoastType() }()
    internal lazy var descriptionTextField: COFInputTextFieldView = { createDescription() }()
    internal lazy var imageInputView: COFInputImageView = { createImageInput() }()
    internal lazy var saveButton: UIButton = { createSaveButton() }()
    
    // MARK: - Sections
    
    private lazy var textSectionView: COFInputSectionView = { createTextSection() }()
    private lazy var imageSectionView: COFInputSectionView = { createImageSection() }()
    private lazy var descriptionSectionView: COFInputSectionView = { createDescriptionSection() }()
    
    // MARK: - Init and Methods
    
    internal weak var presenter: COFCreateCoffeePresenter?
    private var cancellables = Set<AnyCancellable>()
    
    init(presenter: COFCreateCoffeePresenter) {
        self.presenter = presenter
        super.init(frame: .zero)
        setupView()
        setupBindings()
        setupTextFieldBindings()
        setupActions()
        setupKeyboardBindings()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = .systemGroupedBackground
        scrollView.attachToSafeArea(rootView: self)
        scrollView.addArrangeViews([
            textSectionView,
            imageSectionView,
            descriptionSectionView,
            saveButton
        ])
        scrollView.setSubviewsEqualWidthToStackView()
        moveErrorBannerToTop()
    }
    
    private func setupActions() {
        saveButton.addTarget(self, action: #selector(onSaveTap), for: .touchUpInside)
    }
    
    private func setupBindings() {
        guard let presenter = presenter else { return }

        presenter.$name.removeDuplicates()
            .assign(to: \.text, on: nameTextField)
            .store(in: &cancellables)
        
        presenter.$origin.removeDuplicates()
            .assign(to: \.text, on: originTextField)
            .store(in: &cancellables)
        
        presenter.$variety.removeDuplicates()
            .assign(to: \.text, on: varietyTextField)
            .store(in: &cancellables)

        presenter.$roastType.removeDuplicates()
            .assign(to: \.text, on: roastTextField)
            .store(in: &cancellables)

        presenter.$description.removeDuplicates()
            .assign(to: \.text, on: descriptionTextField)
            .store(in: &cancellables)
        
        presenter.$photo.removeDuplicates()
            .assign(to: \.selectedImage, on: imageInputView)
            .store(in: &cancellables)

        presenter.$error
            .sink { [weak self] error in
                self?.error = error
            }
            .store(in: &cancellables)

        Publishers.CombineLatest(presenter.$name, presenter.$roastType)
            .map { (name, roast) -> Bool in
                guard let name = name, let roast = roast else { return false }
                return !name.isEmpty && !roast.isEmpty
            }
            .assign(to: \.isEnabled, on: saveButton)
            .store(in: &cancellables)
    }
    
    private func setupTextFieldBindings() {
        nameTextField.onReturn
            .sink { [weak self] _ in
                self?.nameTextField.setAsFirstResponse(false)
                self?.roastTextField.setAsFirstResponse(true)
            }
            .store(in: &cancellables)
        
        roastTextField.onReturn
            .sink { [weak self] _ in
                self?.roastTextField.setAsFirstResponse(false)
                self?.originTextField.setAsFirstResponse(true)
            }
            .store(in: &cancellables)
        
        
        originTextField.onReturn
            .sink { [weak self] _ in
                self?.originTextField.setAsFirstResponse(false)
                self?.varietyTextField.setAsFirstResponse(true)
            }
            .store(in: &cancellables)
        
        varietyTextField.onReturn
            .sink { [weak self] _ in
                self?.varietyTextField.setAsFirstResponse(false)
            }
            .store(in: &cancellables)
        
        descriptionTextField.onReturn
            .sink { [weak self] _ in
                self?.descriptionTextField.setAsFirstResponse(false)
            }
            .store(in: &cancellables)
    }
    
    private func setupKeyboardBindings() {
        NotificationCenter.default.publisher(for: UIResponder.keyboardWillShowNotification)
            .compactMap { notification -> CGRect? in
                notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect
            }
            .sink { [weak self] rect in
                self?.scrollView.setContentInset(UIEdgeInsets(top: 0,
                                                              left: 0,
                                                              bottom: rect.height,
                                                              right: 0))
            }
            .store(in: &cancellables)

        NotificationCenter.default.publisher(for: UIResponder.keyboardWillHideNotification)
            .sink { [weak self] _ in
                self?.scrollView.setContentInset(.zero)
            }
            .store(in: &cancellables)
    }

    @objc private func onImageTap() {
        let bundle = Bundle(for: COFCreateCoffeeView.self)
        let image = UIImage(named: "CoffeeMock", in: bundle, with: nil)
        imageInputView.selectedImage = image?.jpegData(compressionQuality: 1)
    }

    @objc private func onSaveTap() {
        _onSaveAction.send(())
    }
}

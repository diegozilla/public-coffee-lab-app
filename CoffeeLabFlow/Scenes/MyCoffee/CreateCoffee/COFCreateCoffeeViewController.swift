//
//  COFCreateCoffeeViewController.swift
//  CoffeeLabFlow
//
//  Created by Diego Espinoza on 11/12/22.
//

import UIKit
import Combine
import CoffeeLabBL

protocol COFCreateCoffeeCoordinator: AnyObject {
    func onSaveTapped()
}

final class COFCreateCoffeeViewController: UIViewController {
    private lazy var presenter = COFCreateCoffeePresenter(isUpdate: self.coffeeUUID != nil)
    private lazy var rootView = COFCreateCoffeeView(presenter: self.presenter)
    private let myCoffeeRepositoryService: COFMyCoffeeRepositoryServiceType
    private let coffeeUUID: UUID?
    
    private weak var flowCoordinator: COFCreateCoffeeCoordinator?
    private var cancellables = Set<AnyCancellable>()
    
    override func loadView() {
        view = rootView
    }
    
    init(coffeeUUID: UUID? = nil,
         flowCoordinator: COFCreateCoffeeCoordinator,
         myCoffeeRepositoryService: COFMyCoffeeRepositoryServiceType) {
        self.coffeeUUID = coffeeUUID
        self.flowCoordinator = flowCoordinator
        self.myCoffeeRepositoryService = myCoffeeRepositoryService
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBindings()
        setupData()
    }
    
    @MainActor private func setupData() {
        guard let coffeeUUID = coffeeUUID else { return }
        Task {
            do {
                let coffee = try await myCoffeeRepositoryService.fetch(with: coffeeUUID)
                presenter.name = coffee.name
                presenter.roastType = coffee.roastType
                presenter.origin = coffee.origin
                presenter.variety = coffee.variety
                presenter.description = coffee.description
                presenter.photo = coffee.photo
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    private func setupBindings() {
        rootView
            .nameTextChange
            .removeDuplicates()
            .assign(to: \.name, on: presenter)
            .store(in: &cancellables)
        
        rootView
            .originTextChange
            .removeDuplicates()
            .assign(to: \.origin, on: presenter)
            .store(in: &cancellables)
        
        rootView
            .roastTextChange
            .removeDuplicates()
            .assign(to: \.roastType, on: presenter)
            .store(in: &cancellables)
        
        rootView
            .descriptionTextChange
            .removeDuplicates()
            .assign(to: \.description, on: presenter)
            .store(in: &cancellables)
        
        rootView
            .varietyTextChange
            .removeDuplicates()
            .assign(to: \.variety, on: presenter)
            .store(in: &cancellables)
        
        rootView
            .imageAction
            .receive(on: DispatchQueue.main)
            .sink { [weak self] _ in
                
                // TODO: - this will show a photo picker or camera
                let bundle = Bundle(for: COFCreateCoffeeView.self)
                let image = UIImage(named: "CoffeeMock2", in: bundle, with: nil)
                self?.presenter.photo = image?.jpegData(compressionQuality: 1)
            }
            .store(in: &cancellables)

        rootView
            .onSaveAction
            .sink { [weak self] _ in
                self?.saveCoffee()
            }
            .store(in: &cancellables)
    }
    
    private func saveCoffee() {
        let myCoffee = COFMyCoffee(name: presenter.name ?? "",
                                   roastType: presenter.roastType ?? "",
                                   origin: presenter.origin,
                                   variety: presenter.variety,
                                   description: presenter.description,
                                   photo: presenter.photo,
                                   isFavorite: false)

        Task(priority: .userInitiated) {
            do {
                presenter.isLoading = true
                if let coffeeUUID = self.coffeeUUID {
                    try await myCoffeeRepositoryService.update(coffee: myCoffee, with: coffeeUUID)
                } else {
                    try await myCoffeeRepositoryService.save(coffee: myCoffee)
                }
                
                self.flowCoordinator?.onSaveTapped()
            } catch {
                presenter.isLoading = false
                presenter.error = error
            }
        }
    }
}

//
//  COFCreateCoffeeView+Children.swift
//  CoffeeLabFlow
//
//  Created by Diego Espinoza on 13/12/22.
//

import UIKit
import CoffeeLabUI
import Combine

extension COFCreateCoffeeView {
    func createTextSection() -> COFInputSectionView {
        let section = COFInputSectionView(views: [
            nameTextField,
            roastTextField,
            originTextField,
            varietyTextField
        ])
        section.backgroundColor = .secondarySystemGroupedBackground
        return section
    }
    
    func createImageSection() -> COFInputSectionView {
        let section = COFInputSectionView(views: [
            imageInputView
        ])
        section.backgroundColor = .secondarySystemGroupedBackground
        return section
    }
    
    func createDescriptionSection() -> COFInputSectionView {
        let section = COFInputSectionView(views: [
            descriptionTextField
        ])
        section.backgroundColor = .secondarySystemGroupedBackground
        return section
    }
    
    func createName() -> COFInputTextFieldView {
        let textField = COFInputTextFieldView(title: "Name",
                                              text: nil,
                                              placeholder: "Enter coffee name")
        textField.returnKey = .next
        textField.backgroundColor = .secondarySystemGroupedBackground
        return textField
    }
    
    func createRoastType() -> COFInputTextFieldView {
        let textField = COFInputTextFieldView(title: "Roast Type",
                                              text: nil,
                                              placeholder: "Enter the roast type")
        textField.returnKey = .next
        textField.backgroundColor = .secondarySystemGroupedBackground
        return textField
    }
    
    func createOrigin() -> COFInputTextFieldView {
        let textField = COFInputTextFieldView(title: "Origin",
                                              text: nil,
                                              placeholder: "Enter the origin of the coffee (Optional)")
        textField.returnKey = .next
        textField.backgroundColor = .secondarySystemGroupedBackground
        return textField
    }
    
    func createVariety() -> COFInputTextFieldView {
        let textField = COFInputTextFieldView(title: "Variety",
                                              text: nil,
                                              placeholder: "Enter coffee variety (Optional)")
        textField.returnKey = .done
        textField.backgroundColor = .secondarySystemGroupedBackground
        return textField
    }
    
    func createDescription() -> COFInputTextFieldView {
        let textField = COFInputTextFieldView(title: "Description",
                                              text: nil,
                                              placeholder: "Enter some description (Optional)")
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.returnKey = .done
        textField.backgroundColor = .secondarySystemGroupedBackground
        return textField
    }
    
    func createImageInput() -> COFInputImageView {
        let imageView = COFInputImageView()
        imageView.title = "Coffee Photo"
        imageView.isUserInteractionEnabled = true
        imageView.placeholder = "Select an image"
        return imageView
    }
    
    func createSaveButton() -> UIButton {
        let button = UIButton(type: .system)
        var configuration = UIButton.Configuration.filled()
        configuration.title = presenter?.actionTitle
        configuration.contentInsets = .init(top: .medium, leading: 0, bottom: .medium, trailing: 0)
        button.configuration = configuration
        return button
    }
}


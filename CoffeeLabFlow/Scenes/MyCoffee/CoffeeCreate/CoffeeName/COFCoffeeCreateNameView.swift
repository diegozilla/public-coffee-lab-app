//
//  COFCoffeeCreateNameView.swift
//  CoffeeLabFlow
//
//  Created by Diego Espinoza on 26/01/23.
//

import UIKit
import Combine
import CoffeeLabUI

final class COFCoffeeCreateNameView: COFCoffeeCreateView {
    private lazy var nameTextField: COFTextFieldView = {
        .init(title: "Coffee Name", placeholder: "Enter Coffee Name")
    }()
    private lazy var roastTextField: COFTextFieldView = {
        .init(title: "Coffee Roast", placeholder: "Enter Coffee Roast")
    }()
    
    override func setupView() {
        stackView.addArrangedSubview(nameTextField)
        stackView.addArrangedSubview(roastTextField)
        super.setupView()
    }
    
    override func setupBindings() {
        nameTextField.bind(to: viewModel.name,
                           storeIn: &cancellables)
        
        roastTextField.bind(to: viewModel.roast,
                           storeIn: &cancellables)
        
        viewModel.isNameEnabled
            .sink { [weak self] isEnabled in
                self?.actionButton.isEnabled = isEnabled
            }
            .store(in: &cancellables)
        
        actionButton.onAction
            .sink { [weak self] in
                self?.viewModel.showSetMoreInformation()
            }
            .store(in: &cancellables)
    }
}

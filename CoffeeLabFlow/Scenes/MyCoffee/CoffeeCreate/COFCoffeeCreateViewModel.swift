//
//  COFCoffeeCreateViewModel.swift
//  CoffeeLabFlow
//
//  Created by Diego Espinoza on 26/01/23.
//

import Foundation
import CoffeeLabBL
import Combine

protocol COFCoffeeCreateViewModelType {
    var name: BindableProperty<String?> { get }
    var origin: BindableProperty<String?> { get }
    var roast: BindableProperty<String?> { get }
    var variety: BindableProperty<String?> { get }
    var description: BindableProperty<String?> { get }

    var isNameEnabled: BindableProperty<Bool> { get }
    var isMoreInformationEnabled: BindableProperty<Bool> { get }
    var isImageEnabled: BindableProperty<Bool> { get }
    
    var isUpdating: Bool { get }
    
    func loadCoffee()
    func saveCoffee()
    
    func showSetMoreInformation()
    func showSetImage()
}

class COFCoffeeCreateViewModel: COFCoffeeCreateViewModelType {
    var name: BindableProperty<String?> = .init(nil)
    var origin: BindableProperty<String?> = .init(nil)
    var roast: BindableProperty<String?> = .init(nil)
    var variety: BindableProperty<String?> = .init(nil)
    var description: BindableProperty<String?> = .init(nil)
    
    var isNameEnabled: BindableProperty<Bool> = .init(false)
    var isMoreInformationEnabled: BindableProperty<Bool> = .init(false)
    var isImageEnabled: BindableProperty<Bool> = .init(true)
    
    var isUpdating: Bool { coffeeId != nil }

    private weak var coordinator: (any COFCoffeeCreateCoordinator)?
    private var cancellables = Set<AnyCancellable>()
    private let coffeeId: UUID?
    private let myCoffeeRepositoryService: any COFMyCoffeeRepositoryServiceType

    init(coffeeId: UUID? = nil,
         myCoffeeRepositoryService: COFMyCoffeeRepositoryServiceType,
         coordinator: COFCoffeeCreateCoordinator) {
        self.coffeeId = coffeeId
        self.myCoffeeRepositoryService = myCoffeeRepositoryService
        self.coordinator = coordinator
        setupBindings()
    }
    
    private func setupBindings() {
        Publishers.CombineLatest3(origin, description, variety)
            .map { origin, description, variety in
                guard let origin = origin, let description = description, let variety = variety else { return false }
                return origin != "" && description != "" && variety != ""
            }
            .subscribe(isMoreInformationEnabled)
            .store(in: &cancellables)
        
        Publishers.CombineLatest(name, roast)
            .map { name, roast in
                guard let name = name, let roast = roast else { return false }
                return name != "" && roast != ""
            }
            .subscribe(isNameEnabled)
            .store(in: &cancellables)
    }
    
    @MainActor func loadCoffee() {
        guard let coffeeId = coffeeId else { return }
        Task(priority: .userInitiated) {
            do {
                let coffee = try await myCoffeeRepositoryService.fetch(with: coffeeId)
                self.name.send(coffee.name)
                self.roast.send(coffee.roastType)
                self.origin.send(coffee.origin)
                self.variety.send(coffee.variety)
                self.description.send(coffee.description)
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    @MainActor func saveCoffee() {
        guard let name = name.value,
              let roast = roast.value,
              let origin = origin.value,
              let variety = variety.value,
              let description = description.value else {
            // TODO: - show error message
            return
        }
        
        let myCoffee = COFMyCoffee(name: name,
                                   roastType: roast,
                                   origin: origin,
                                   variety: variety,
                                   description: description,
                                   photo: nil,
                                   isFavorite: false)

        Task(priority: .userInitiated) {
            do {
                // set the state loading
                if let coffeeId = self.coffeeId {
                    try await myCoffeeRepositoryService.update(coffee: myCoffee, with: coffeeId)
                } else {
                    try await myCoffeeRepositoryService.save(coffee: myCoffee)
                }
                self.coordinator?.onCreateImageContinue()
            } catch {
                // set error message
            }
        }
    }
    
    func showSetMoreInformation() {
        coordinator?.onCreateNameContinue(viewModel: self)
    }

    func showSetImage() {
        coordinator?.onCreateInformationContinue(viewModel: self)
    }
}

//
//  COFCoffeeCreateInformationView.swift
//  CoffeeLabFlow
//
//  Created by Diego Espinoza on 26/01/23.
//

import UIKit
import CoffeeLabUI

final class COFCoffeeCreateInformationView: COFCoffeeCreateView {
    private lazy var originTextField: COFTextFieldView = {
        .init(title: "Coffee Origin", placeholder: "Enter Coffee Origin")
    }()
    private lazy var varietyTextField: COFTextFieldView = {
        .init(title: "Coffee Variety", placeholder: "Enter Coffee Variety")
    }()
    private lazy var descriptionTextField: COFTextFieldView = {
        .init(title: "Coffee Description", placeholder: "Enter Coffee Description")
    }()
    
    override func setupView() {
        stackView.addArrangedSubview(originTextField)
        stackView.addArrangedSubview(varietyTextField)
        stackView.addArrangedSubview(descriptionTextField)
        super.setupView()
    }
    
    override func setupBindings() {
        originTextField.bind(to: viewModel.origin,
                             storeIn: &cancellables)
        varietyTextField.bind(to: viewModel.variety,
                              storeIn: &cancellables)
        descriptionTextField.bind(to: viewModel.description,
                                  storeIn: &cancellables)
        
        viewModel.isMoreInformationEnabled
            .sink { [weak self] isEnabled in
                self?.actionButton.isEnabled = isEnabled
            }
            .store(in: &cancellables)
        
        actionButton
            .onAction
            .sink { [weak self] in
                self?.viewModel.showSetImage()
            }
            .store(in: &cancellables)
    }
}


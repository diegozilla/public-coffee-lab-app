//
//  COFCoffeeCreateInformationViewController.swift
//  CoffeeLabFlow
//
//  Created by Diego Espinoza on 26/01/23.
//

import UIKit

final class COFCoffeeCreateInformationViewController: UIViewController {
    private let rootView: COFCoffeeCreateInformationView
    private let viewModel: COFCoffeeCreateViewModelType
    
    init(viewModel: COFCoffeeCreateViewModelType,
         rootView: COFCoffeeCreateInformationView) {
        self.viewModel = viewModel
        self.rootView = rootView
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = rootView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

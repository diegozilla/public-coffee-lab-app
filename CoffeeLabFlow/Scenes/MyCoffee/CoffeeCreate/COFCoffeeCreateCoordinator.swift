//
//  COFCoffeeCreateCoordinator.swift
//  CoffeeLabFlow
//
//  Created by Diego Espinoza on 27/01/23.
//

import Foundation

protocol COFCoffeeCreateCoordinator: AnyObject {
    func onCreateNameContinue(viewModel: COFCoffeeCreateViewModelType)
    func onCreateInformationContinue(viewModel: COFCoffeeCreateViewModelType)
    func onCreateImageContinue()
}

//
//  COFCoffeeCreateView.swift
//  CoffeeLabFlow
//
//  Created by Diego Espinoza on 27/01/23.
//

import UIKit
import Combine
import CoffeeLabUI

open class COFCoffeeCreateView: UIView {
    lazy var actionButton: COFButton = { .filled(title: actionTitle) }()
    lazy var stackView: UIStackView = { .createVStackView() }()
    var cancellables = Set<AnyCancellable>()
    
    let viewModel: COFCoffeeCreateViewModelType
    
    open var actionTitle: String { "Continue" }
    
    init(viewModel: COFCoffeeCreateViewModelType) {
        self.viewModel = viewModel
        super.init(frame: .zero)
        setupView()
        setupBindings()
    }
    
    public required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open func setupView() {
        backgroundColor = .systemBackground
        addSubview(stackView)
        NSLayoutConstraint.activate([
            stackView.attachTop(rootView: self, constant: .medium),
            stackView.attachLeading(rootView: self, constant: .medium),
            stackView.attachTrailing(rootView: self, constant: -.medium)
        ])
        if let lastSubview = stackView.arrangedSubviews.last {
            stackView.setCustomSpacing(.large, after: lastSubview)
        }
        stackView.addArrangedSubview(actionButton)
    }
    
    open func setupBindings() { }
}

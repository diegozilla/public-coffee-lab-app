//
//  COFCoffeeCreateImageViewController.swift
//  CoffeeLabFlow
//
//  Created by Diego Espinoza on 26/01/23.
//

import UIKit

final class COFCoffeeCreateImageViewController: UIViewController {
    private let rootView: COFCoffeeCreateImageView
    private let viewModel: COFCoffeeCreateViewModelType
    
    init(viewModel: COFCoffeeCreateViewModelType,
         rootView: COFCoffeeCreateImageView) {
        self.viewModel = viewModel
        self.rootView = rootView
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = rootView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

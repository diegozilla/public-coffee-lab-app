//
//  COFCoffeeCreateImageView.swift
//  CoffeeLabFlow
//
//  Created by Diego Espinoza on 26/01/23.
//

import UIKit
import CoffeeLabUI

final class COFCoffeeCreateImageView: COFCoffeeCreateView {
    override func setupView() {
        let temporalLabel = COFLabel(text: "Not implemented yet, just continue", configuration: .title2)
        stackView.addArrangedSubview(temporalLabel)
        super.setupView()
    }
    
    override func setupBindings() {
        actionButton
            .onAction
            .sink { [weak self] in
                self?.viewModel.saveCoffee()
            }
            .store(in: &cancellables)
    }
}


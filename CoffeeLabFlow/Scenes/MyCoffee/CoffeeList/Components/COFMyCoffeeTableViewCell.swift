//
//  COFMyCoffeeTableViewCell.swift
//  CoffeeLabFlow
//
//  Created by Diego Espinoza on 11/12/22.
//

import UIKit
import CoffeeLabUI

final class COFMyCoffeeTableViewCell: UITableViewCell {
    var itemModel: COFMyCoffeeItemModel? {
        didSet {
            titleLabel.set(text: itemModel?.name)
            detailLabel.set(text: itemModel?.detail)
            
            if let imageData = itemModel?.presentedImage {
                iconImageView.image = UIImage(data: imageData,
                                              scale: contentView.window?.contentScaleFactor ?? 1)
            }

            if let category = itemModel?.category {
                switch category {
                case .favorite:
                    categoryColorView.backgroundColor = .theme.accent2Color
                case .all:
                    categoryColorView.backgroundColor = .theme.accentColor
                }
            }
        }
    }
    
    private lazy var titleLabel: COFLabel = {
        let label = COFLabel(configuration: .title2)
        return label
    }()

    private lazy var detailLabel: COFLabel = {
        let label = COFLabel(configuration: .bodyDetail)
        return label
    }()

    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        let bundle = Bundle(for: COFMyCoffeeTableViewCell.self)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = .small
        imageView.layer.masksToBounds = true
        imageView.heightAnchor.constraint(equalToConstant: .xxLarge).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: .xxLarge).isActive = true
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    private lazy var categoryColorView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.widthAnchor.constraint(equalToConstant: .tiny).isActive = true
        view.layer.cornerRadius = CGFloat.tiny * 0.5
        return view
    }()

    private lazy var vStackView: UIStackView = {
        .createVStackView(spacing: .tiny)
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupView() {
        accessoryType = .disclosureIndicator
        contentView.addSubview(iconImageView)
        contentView.addSubview(vStackView)
        contentView.addSubview(categoryColorView)
    }
    
    private func setupLayout() {
        NSLayoutConstraint.activate([
            iconImageView.leadingAnchor.constraint(equalTo: categoryColorView.trailingAnchor, constant: .medium),
            iconImageView.centerYAnchor.constraint(equalTo: vStackView.centerYAnchor)
        ])
        
        NSLayoutConstraint.activate([
            vStackView.centerYAnchor.constraint(equalTo: iconImageView.centerYAnchor),
            vStackView.leadingAnchor.constraint(equalTo: iconImageView.trailingAnchor, constant: .medium),
            vStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -.small),
            vStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: .xMedium),
            vStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -.xMedium)
        ])

        NSLayoutConstraint.activate([
            categoryColorView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: .medium),
            categoryColorView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -.medium),
            categoryColorView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: .medium)
        ])

        vStackView.addArrangedSubview(titleLabel)
        vStackView.addArrangedSubview(detailLabel)
    }
}

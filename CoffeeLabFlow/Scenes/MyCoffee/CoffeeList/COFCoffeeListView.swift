//
//  COFCoffeeListView.swift
//  CoffeeLabFlow
//
//  Created by Diego Espinoza on 26/01/23.
//

import UIKit

class COFCoffeeListView: UIView {
    private let viewModel: COFCoffeeListViewModelType

    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(COFMyCoffeeTableViewCell.self)
        tableView.delegate = self
        return tableView
    }()
    
    init(viewModel: COFCoffeeListViewModelType) {
        self.viewModel = viewModel
        super.init(frame: .zero)
        setupView()
        setupViewModel()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViewModel() {
        viewModel.setupDataSource(tableView: tableView)
    }
    
    private func setupView() {
        tableView.attachTo(rootView: self)
    }
}

extension COFCoffeeListView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.coffeeItemSelected(indexPath: indexPath)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let section = viewModel.sections[indexPath.section]
        
        switch section {
        case .all:
            return createMoveToFavorite(indexPath: indexPath)
        case .favorite:
            return createRemoveFromFavorite(indexPath: indexPath)
        case .latest:
            return nil
        }
    }
    
    private func createMoveToFavorite(indexPath: IndexPath) -> UISwipeActionsConfiguration {
        let action = UIContextualAction(style: .normal,
                                        title: "Move to favorites") { [weak self] action, view, completion in
            self?.viewModel.addCoffeeToFavorites(indexPath: indexPath)
            completion(true)
        }
        action.backgroundColor = .systemOrange
        return UISwipeActionsConfiguration(actions: [action])
    }
    
    private func createRemoveFromFavorite(indexPath: IndexPath) -> UISwipeActionsConfiguration {
        let action = UIContextualAction(style: .normal,
                                        title: "Remove from favorites") { [weak self] action, view, completion in
            self?.viewModel.removeCoffeeFromFavorites(indexPath: indexPath)
            completion(true)
        }
        action.backgroundColor = .systemBrown
        return UISwipeActionsConfiguration(actions: [action])
    }
}

//
//  COFCoffeeListViewController.swift
//  CoffeeLabFlow
//
//  Created by Diego Espinoza on 26/01/23.
//

import UIKit

protocol COFCoffeeListCoordinator: AnyObject {
    func onCreateCoffeeTap()
    func onCoffeeItemTap(id: UUID)
}

class COFCoffeeListViewController: UIViewController {
    private let rootView: COFCoffeeListView
    private let viewModel: COFCoffeeListViewModelType
    
    init(viewModel: COFCoffeeListViewModelType,
         rootView: COFCoffeeListView) {
        self.rootView = rootView
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = rootView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupItem()
        viewModel.fetchCoffeeList()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // TODO: - find a way to update coffee list once a new one is added/updated
        viewModel.fetchCoffeeList()
    }

    private func setupItem() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "plus.circle.fill"),
                                                            style: .plain,
                                                            target: self,
                                                            action: #selector(onAddTap))
    }
    
    @objc private func onAddTap() {
        viewModel.showCreateCoffee()
    }
}

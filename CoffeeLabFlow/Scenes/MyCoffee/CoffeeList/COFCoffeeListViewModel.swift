//
//  COFCoffeeListViewModel.swift
//  CoffeeLabFlow
//
//  Created by Diego Espinoza on 26/01/23.
//

import UIKit
import Combine
import Foundation
import CoffeeLabBL

protocol COFCoffeeListViewModelType: AnyObject {
    var coffeeListDataSource: COFCoffeeListDataSource? { get }
    var sections: [COFMyCoffeeSection] { get }

    func setupDataSource(tableView: UITableView)
    func fetchCoffeeList()
    func coffeeItemSelected(indexPath: IndexPath)
    
    func addCoffeeToFavorites(indexPath: IndexPath)
    func removeCoffeeFromFavorites(indexPath: IndexPath)
    func showCoffeeItem(id: UUID)
    func showCreateCoffee()
}

final class COFCoffeeListViewModel: COFCoffeeListViewModelType {
    private let myCoffeeRepositoryService: any COFMyCoffeeRepositoryServiceType
    private weak var coordinator: (any COFCoffeeListCoordinator)?
    private var cancellables = Set<AnyCancellable>()

    var coffeeListDataSource: COFCoffeeListDataSource?
    
    var sections: [COFMyCoffeeSection] {
        guard let snapshot = coffeeListDataSource?.snapshot() else { return [] }
        return snapshot.sectionIdentifiers
    }
    
    init(myCoffeeRepositoryService: COFMyCoffeeRepositoryServiceType,
         coordinator: COFCoffeeListCoordinator) {
        self.myCoffeeRepositoryService = myCoffeeRepositoryService
        self.coordinator = coordinator
    }

    func setupDataSource(tableView: UITableView) {
        coffeeListDataSource = COFCoffeeListDataSource(tableView: tableView)
        tableView.dataSource = coffeeListDataSource
    }
    
    func fetchCoffeeList() {
        Task {
            do {
                let myCoffee = try await myCoffeeRepositoryService.fetch()
                let favoriteItems: [COFMyCoffeeItemModel] = myCoffee.filter { $0.isFavorite }.map {
                    .init(myCoffee: $0, category: .favorite)
                }
                let items: [COFMyCoffeeItemModel] = myCoffee.filter { !$0.isFavorite }.map {
                    .init(myCoffee: $0, category: .all)
                }
                await coffeeListDataSource?.setCoffee(allItems: items,
                                                      favoriteItems: favoriteItems)
            } catch {
                //self.error = error
            }
        }
    }
    
    func coffeeItemSelected(indexPath: IndexPath) {
        guard let dataSource = coffeeListDataSource else {
            // TODO: - show error
            return
        }
        let snapshot = dataSource.snapshot()
        let section = snapshot.sectionIdentifiers[indexPath.section]
        let item = dataSource.item(forIndexPath: indexPath)

        switch section {
        case .favorite:
            showCoffeeItem(id: item.uuid)
        case .all:
            showCoffeeItem(id: item.uuid)
        case .latest:
            fatalError("Not supported yet")
        }
    }
    
    func addCoffeeToFavorites(indexPath: IndexPath) {
        guard let coffee = coffeeListDataSource?.item(forIndexPath: indexPath) else { return }
        Task {
            do {
                var currentCoffee = try await myCoffeeRepositoryService.fetch(with: coffee.uuid)
                currentCoffee.isFavorite = true
                try await myCoffeeRepositoryService.update(coffee: currentCoffee, with: coffee.uuid)
                let newCoffee = COFMyCoffeeItemModel(cellId: coffee.cellId,
                                                     myCoffee: currentCoffee,
                                                     category: .favorite)
                await coffeeListDataSource?.addCoffeeToFavorite(coffee: newCoffee)
            } catch {
                print("ERROR?!")
                // TODO: - set error
            }
        }
    }
    
    func removeCoffeeFromFavorites(indexPath: IndexPath) {
        guard let coffee = coffeeListDataSource?.item(forIndexPath: indexPath) else { return }
        Task {
            do {
                var currentCoffee = try await myCoffeeRepositoryService.fetch(with: coffee.uuid)
                currentCoffee.isFavorite = false
                try await myCoffeeRepositoryService.update(coffee: currentCoffee, with: coffee.uuid)
                let newCoffee = COFMyCoffeeItemModel(cellId: coffee.cellId,
                                                     myCoffee: currentCoffee,
                                                     category: .all)
                await coffeeListDataSource?.removeCoffeeFromFavorite(coffee: newCoffee)
            } catch {
                print("ERROR?!")
                // TODO: - set error
            }
        }
    }
    
    func showCreateCoffee() {
        coordinator?.onCreateCoffeeTap()
    }
    
    func showCoffeeItem(id: UUID) {
        coordinator?.onCoffeeItemTap(id: id)
    }
}

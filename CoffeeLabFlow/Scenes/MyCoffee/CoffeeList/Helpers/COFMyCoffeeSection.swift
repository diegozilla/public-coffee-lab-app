//
//  COFMyCoffeeSection.swift
//  CoffeeLabFlow
//
//  Created by Diego Espinoza on 13/12/22.
//

import UIKit
import CoffeeLabBL

enum COFMyCoffeeSection {
    case favorite
    case latest
    case all

    var sectionTitle: String {
        switch self {
        case .favorite:
            return "Favorite coffees"
        case .latest:
            return "Latest used coffees"
        case .all:
            return "All other coffees"
        }
    }
}

struct COFMyCoffeeItemModel: Identifiable, Hashable, Equatable {
    enum Category {
        case all
        case favorite
    }

    var cellId: UUID
    let uuid: UUID
    let name: String
    let detail: String
    let image: Data?
    var category: Category
    let isFavorite: Bool
    
    var id: UUID { cellId }

    init(cellId: UUID? = nil, myCoffee: COFMyCoffee, category: Category) {
        self.cellId = cellId ?? UUID()
        self.name = myCoffee.name
        self.detail = myCoffee.roastType
        self.image = myCoffee.photo
        self.category = category
        self.uuid = myCoffee.uuid
        self.isFavorite = myCoffee.isFavorite
    }
    
    var presentedImage: Data? {
        let bundle = Bundle(for: COFCoffeeListDataSource.self)
        let mockImage = UIImage(named: "CoffeeMock", in: bundle, with: nil)
        return image ?? mockImage?.jpegData(compressionQuality: 0.5)
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(cellId)
    }
    
    static func == (lhs: COFMyCoffeeItemModel, rhs: COFMyCoffeeItemModel) -> Bool {
        lhs.cellId == rhs.cellId
    }
}

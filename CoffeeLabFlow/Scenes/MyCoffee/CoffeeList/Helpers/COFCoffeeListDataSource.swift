//
//  COFCoffeeListDataSource.swift
//  CoffeeLabFlow
//
//  Created by Diego Espinoza on 26/01/23.
//

import UIKit

class COFCoffeeListDataSource: UITableViewDiffableDataSource<COFMyCoffeeSection, COFMyCoffeeItemModel> {
    var favorites: [COFMyCoffeeItemModel] {
        let snapshot = snapshot()
        let favorites = snapshot.itemIdentifiers(inSection: .favorite)
        return favorites
    }
    
    var all: [COFMyCoffeeItemModel] {
        let snapshot = snapshot()
        let favorites = snapshot.itemIdentifiers(inSection: .all)
        return favorites
    }
    
    init(tableView: UITableView) {
        super.init(tableView: tableView) { tableView, indexPath, itemIdentifier in
            let cell = tableView.reuse(COFMyCoffeeTableViewCell.self, indexPath)
            cell.itemModel = itemIdentifier
            return cell
        }
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let snapshot = snapshot()
        let section = snapshot.sectionIdentifiers[section]
        return section.sectionTitle
    }
    
    func item(forIndexPath indexPath: IndexPath) -> COFMyCoffeeItemModel {
        let snapshot = snapshot()
        let section = snapshot.sectionIdentifiers[indexPath.section]
        let item = snapshot.itemIdentifiers(inSection: section)[indexPath.item]
        return item
    }
    
    func setCoffee(allItems: [COFMyCoffeeItemModel], favoriteItems: [COFMyCoffeeItemModel]) {
        var snapshot = snapshot()
        snapshot.deleteAllItems()
        
        if !favoriteItems.isEmpty {
            snapshot.appendSections([.favorite])
            snapshot.appendItems(favoriteItems)
        }
        
        if !allItems.isEmpty {
            snapshot.appendSections([.all])
            snapshot.appendItems(allItems)
        }
        
        apply(snapshot, animatingDifferences: false)
    }
    
    func addCoffeeToFavorite(coffee: COFMyCoffeeItemModel) {
        var snapshot = snapshot()
        snapshot.deleteItems([coffee])
        
        let itemsInAll = snapshot.itemIdentifiers(inSection: .all)
        if itemsInAll.isEmpty {
            snapshot.deleteSections([.all])
        }
        
        let indexOfAll = snapshot.indexOfSection(.favorite)
        if indexOfAll == nil {
            snapshot.insertSections([.favorite], beforeSection: .all)
        }
        snapshot.appendItems([coffee], toSection: .favorite)
        apply(snapshot, animatingDifferences: true)
        
        // This is necessary for refreshing/reloading the UI from the cell
        //DispatchQueue.main.async {
            snapshot.reloadItems([coffee])
            self.apply(snapshot, animatingDifferences: false)
        //}
    }
    
    func removeCoffeeFromFavorite(coffee: COFMyCoffeeItemModel) {
        var snapshot = snapshot()
        snapshot.deleteItems([coffee])
        
        let itemsInAll = snapshot.itemIdentifiers(inSection: .favorite)
        if itemsInAll.isEmpty {
            snapshot.deleteSections([.favorite])
        }
        
        let indexOfAll = snapshot.indexOfSection(.all)
        if indexOfAll == nil {
            snapshot.insertSections([.all], afterSection: .favorite)
        }
        snapshot.appendItems([coffee], toSection: .all)
        apply(snapshot, animatingDifferences: true)
        
        // This is necessary for refreshing/reloading the UI from the cell
        //DispatchQueue.main.async {
            snapshot.reloadItems([coffee])
            self.apply(snapshot, animatingDifferences: false)
        //}
    }
}

//
//  COFAddCoffeeNameView.swift
//  CoffeeLabFlow
//
//  Created by Diego Espinoza on 17/12/22.
//

import UIKit
import Combine
import CoffeeLabUI

extension COFAddCoffeeNameView {
    final class Presenter {
        @PropertyBinding var name: String?
        @PropertyBinding var roast: String?
        
        @Published var isEnabled = false
    }
}

final class COFAddCoffeeNameView: COFAddCoffeeView {
    override var actionTitle: String? { "Continue" }
    
    let presenter = Presenter()
    
    private lazy var nameTextField: COFTextFieldView = {
        return COFTextFieldView(title: "Name",
                                placeholder: "Enter the name")
    }()

    private lazy var roastTextField: COFTextFieldView = {
        COFTextFieldView(title: "Roast Type",
                         placeholder: "Enter the roast")
    }()
    
    private let _onContinue: PassthroughSubject<Void, Never> = .init()
    
    init() {
        super.init(frame: .zero)
    }
    
    public required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setupView() {
        backgroundColor = .systemBackground
        scrollView.addArrangeViews([nameTextField, roastTextField])
        super.setupView()
    }
    
    override func setupBindings() {
        presenter.$isEnabled
            .sink { [weak self] isEnabled in
                self?.isActionEnabled = isEnabled
            }
            .store(in: &cancellables)
        
        nameTextField.bind(to: presenter.$name,
                           storeIn: &cancellables)

        roastTextField.bind(to: presenter.$roast,
                            storeIn: &cancellables)

        Publishers.CombineLatest(nameTextField.onTextChange, roastTextField.onTextChange)
            .map { name, roast -> Bool in
                guard let name = name, let roast = roast else { return false }
                return !name.isEmpty && !roast.isEmpty
            }
            .sink { [weak self] isValid in
                self?.presenter.isEnabled = isValid
            }
            .store(in: &cancellables)
    }
}

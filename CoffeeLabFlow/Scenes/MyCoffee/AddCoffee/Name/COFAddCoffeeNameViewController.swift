//
//  COFAddCoffeeNameViewController.swift
//  CoffeeLabFlow
//
//  Created by Diego Espinoza on 17/12/22.
//

import UIKit
import Combine
import CoffeeLabBL

protocol COFAddCoffeeNameCoordinator: AnyObject {
    func nameContinueTapped(form: COFAddCoffeeForm, coffeeId: UUID?)
}

final class COFAddCoffeeNameViewController<C>: UIViewController where C: COFAddCoffeeNameCoordinator {
    private let myCoffeeRepositoryService: COFMyCoffeeRepositoryServiceType

    private weak var coordinator: C?
    private lazy var rootView = COFAddCoffeeNameView()
    private var presenter: COFAddCoffeeNameView.Presenter { rootView.presenter }
    private var cancellables = Set<AnyCancellable>()
    
    private var coffeeId: UUID?
    private var form = COFAddCoffeeForm()
    
    init(coffeeId: UUID?, coordinator: C, myCoffeeRepositoryService: COFMyCoffeeRepositoryServiceType) {
        self.myCoffeeRepositoryService = myCoffeeRepositoryService
        self.coffeeId = coffeeId
        self.coordinator = coordinator
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = rootView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBindings()
        setupData()
    }
    
    private func setupData() {
        guard let coffeeId = coffeeId else { return }
        Task(priority: .userInitiated) {
            do {
                let coffee = try await myCoffeeRepositoryService.fetch(with: coffeeId)
                form.name = coffee.name
                form.roast = coffee.roastType
                form.origin = coffee.origin ?? ""
                form.variety = coffee.variety ?? ""
                form.description = coffee.description ?? ""
                
                presenter.name = form.name
                presenter.roast = form.roast
                
                presenter.isEnabled = !form.name.isEmpty && !form.roast.isEmpty
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    private func setupBindings() {
        rootView.onContinue
            .sink { [weak self] _ in
                guard let owner = self else { return }
                var form = owner.form
                form.name = owner.presenter.name ?? ""
                form.roast = owner.presenter.roast ?? ""
                self?.coordinator?.nameContinueTapped(form: form, coffeeId: owner.coffeeId)
            }
            .store(in: &cancellables)
    }
}

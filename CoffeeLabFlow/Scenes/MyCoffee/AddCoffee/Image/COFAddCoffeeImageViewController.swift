//
//  COFAddCoffeeImageViewController.swift
//  CoffeeLabFlow
//
//  Created by Diego Espinoza on 4/01/23.
//

import UIKit
import Combine
import CoffeeLabBL

protocol COFAddCoffeeImageViewCoordinator: AnyObject {
    func saveTapped()
}

final class COFAddCoffeeImageViewController<C>: UIViewController where C: COFAddCoffeeImageViewCoordinator {
    private let myCoffeeRepositoryService: COFMyCoffeeRepositoryServiceType

    private var rootView = COFAddCoffeeImageView()
    private weak var coordinator: C?
    private var cancellables = Set<AnyCancellable>()
    
    private let form: COFAddCoffeeForm
    private let coffeeId: UUID?
    
    init(coffeeId: UUID?, form: COFAddCoffeeForm, coordinator: C, myCoffeeRepositoryService: COFMyCoffeeRepositoryServiceType) {
        self.form = form
        self.coffeeId = coffeeId
        self.myCoffeeRepositoryService = myCoffeeRepositoryService
        self.coordinator = coordinator
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = rootView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBindings()
    }
    
    private func setupBindings() {
        rootView.onContinue
            .sink { [weak self] _ in
                self?.save()
            }
            .store(in: &cancellables)
    }
    
    private func save() {
        let myCoffee = COFMyCoffee(name: form.name,
                                   roastType: form.roast,
                                   origin: form.origin,
                                   variety: form.variety,
                                   description: form.description,
                                   photo: nil,
                                   isFavorite: false)

        Task(priority: .userInitiated) {
            do {
//                //presenter.isLoading = true
                if let coffeeId = self.coffeeId {
                    try await myCoffeeRepositoryService.update(coffee: myCoffee, with: coffeeId)
                } else {
                    try await myCoffeeRepositoryService.save(coffee: myCoffee)
                }
                self.coordinator?.saveTapped()
            } catch {
                //presenter.isLoading = false
                //presenter.error = error
            }
        }
    }
}

//
//  COFAddCoffeeImageView.swift
//  CoffeeLabFlow
//
//  Created by Diego Espinoza on 4/01/23.
//

import UIKit
import CoffeeLabUI

final class COFAddCoffeeImageView: COFAddCoffeeView {
    override var actionTitle: String? { "Save" }
    
    override func setupView() {
        backgroundColor = .systemBackground
        super.setupView()
    }
}

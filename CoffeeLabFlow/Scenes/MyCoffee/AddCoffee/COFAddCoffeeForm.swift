//
//  COFAddCoffeeForm.swift
//  CoffeeLabFlow
//
//  Created by Diego Espinoza on 4/01/23.
//

import Foundation

struct COFAddCoffeeForm {
    var name: String = ""
    var roast: String = ""
    
    var variety: String = ""
    var origin: String = ""
    var description: String = ""
}

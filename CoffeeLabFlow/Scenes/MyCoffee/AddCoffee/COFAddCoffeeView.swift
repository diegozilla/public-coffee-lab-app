//
//  COFAddCoffeeView.swift
//  CoffeeLabFlow
//
//  Created by Diego Espinoza on 17/12/22.
//

import UIKit
import Combine
import CoffeeLabUI

open class COFAddCoffeeView: COFView {
    var actionTitle: String? { nil }
    var onContinue: AnyPublisher<Void, Never> { actionButton.onAction }
    var isActionEnabled: Bool {
        get { actionButton.isEnabled }
        set { actionButton.isEnabled = newValue }
    }

    lazy var scrollView: COFScrollStackView = {
        let scrollView = COFScrollStackView(configuration: .mediumVertical,
                                            handleKeyboard: true)
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()

    private lazy var actionButton: COFButton = {
        COFButton.filled(title: actionTitle ?? "")
    }()

    var cancellables = Set<AnyCancellable>()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        setupBindings()
    }

    public required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    internal func setupView() {
        addSubview(scrollView)
        scrollView.attachTo(rootView: self)
        scrollView.addArrangeViews([actionButton])
        scrollView.setSubviewsEqualWidthToStackView()
        moveErrorBannerToTop()
    }
    
    internal func setupBindings() { }
}

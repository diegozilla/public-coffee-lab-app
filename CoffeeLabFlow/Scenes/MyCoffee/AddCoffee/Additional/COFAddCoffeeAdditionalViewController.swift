//
//  COFAddCoffeeAdditionalViewController.swift
//  CoffeeLabFlow
//
//  Created by Diego Espinoza on 22/12/22.
//

import UIKit
import Combine

protocol COFAddCoffeeAdditionalCoordinator: AnyObject {
    func additionalContinueTapped(form: COFAddCoffeeForm, coffeeId: UUID?)
}

final class COFAddCoffeeAdditionalViewController<C>: UIViewController where C: COFAddCoffeeAdditionalCoordinator {
    
    private weak var coordinator: C?
    private lazy var rootView = COFAddCoffeeAdditionalView()
    private var cancellables = Set<AnyCancellable>()

    private var presenter: COFAddCoffeeAdditionalView.Presenter { rootView.presenter }
    private var form: COFAddCoffeeForm
    private let coffeeId: UUID?
    
    init(coffeeId: UUID?, form: COFAddCoffeeForm, coordinator: C) {
        self.coffeeId = coffeeId
        self.form = form
        self.coordinator = coordinator
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = rootView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupBindings()
        setupData()
    }

    private func setupData() {
        guard let coffeeId = coffeeId else { return }
        presenter.origin = form.origin
        presenter.description = form.description
        presenter.variety = form.description
    }
    
    private func setupBindings() {
        rootView.onContinue
            .sink { [weak self] _ in
                guard let owner = self else { return }
                var updatedForm = owner.form
                updatedForm.variety = owner.presenter.variety ?? ""
                updatedForm.origin = owner.presenter.origin ?? ""
                updatedForm.description = owner.presenter.description ?? ""
                self?.coordinator?.additionalContinueTapped(form: updatedForm, coffeeId: owner.coffeeId)
            }
            .store(in: &cancellables)
    }
}

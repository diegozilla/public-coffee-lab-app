//
//  COFAddCoffeeAditionalView.swift
//  CoffeeLabFlow
//
//  Created by Diego Espinoza on 22/12/22.
//

import UIKit
import CoffeeLabUI
import Combine

final class COFAddCoffeeAdditionalView: COFAddCoffeeView {
    
    final class Presenter {
        @PropertyBinding var origin: String?
        @PropertyBinding var variety: String?
        @PropertyBinding var description: String?
    }
    
    override var actionTitle: String? { "Continue" }
    
    let presenter = Presenter()

    private lazy var originTextField: COFTextFieldView = {
        COFTextFieldView(title: "Origin",
                         placeholder: "Enter the origin")
    }()
    
    private lazy var varietyTextField: COFTextFieldView = {
        COFTextFieldView(title: "Variety",
                         placeholder: "Enter the variety")
    }()

    private lazy var descriptionTextField: COFTextFieldView = {
        COFTextFieldView(title: "Description",
                         placeholder: "Enter the description")
    }()

    override func setupView() {
        isActionEnabled = true
        backgroundColor = .systemBackground
        scrollView.addArrangeViews([originTextField, varietyTextField, descriptionTextField])
        super.setupView()
    }
    
    override func setupBindings() {
        originTextField.bind(to: presenter.$origin,
                             storeIn: &cancellables)
        
        varietyTextField.bind(to: presenter.$variety,
                              storeIn: &cancellables)
        
        descriptionTextField.bind(to: presenter.$description,
                                  storeIn: &cancellables)
        
        Publishers.CombineLatest3(originTextField.onTextChange,
                                 varietyTextField.onTextChange,
                                 descriptionTextField.onTextChange)
        .map { origin, variety, description in
            guard let origin = origin, let variety = variety, let description = description else { return false }
            return !origin.isEmpty && !variety.isEmpty && !description.isEmpty
        }
        .sink { [weak self] isValid in
            self?.isActionEnabled = isValid
        }
        .store(in: &cancellables)
    }
}

//
//  COFMyCoffeeDatasource.swift
//  CoffeeLabFlow
//
//  Created by Diego Espinoza on 11/12/22.
//

import UIKit
import Combine
import CoffeeLabBL

final class COFMyCoffeePresenter {
    @Published var myCoffeeItems: [COFMyCoffeeItemModel] = []
    @Published var error: Error?
    
    var favoriteMyCoffeeItems: [COFMyCoffeeItemModel] = []
    
    var sections: [COFMyCoffeeSection] {
        guard let snapshot = myCoffeeDataSource?.snapshot() else { return [] }
        return snapshot.sectionIdentifiers
    }
    
    private(set) var myCoffeeDataSource: COFMyCoffeeDataSource?
    private var cancellables = Set<AnyCancellable>()
    
    init() {
        setupMyCoffeeItems()
    }
    
    private func setupMyCoffeeItems() {
        $myCoffeeItems
            .dropFirst(1)
            .sink { [weak self] items in
                guard let owner = self, let dataSource = owner.myCoffeeDataSource else { return }
                var snapshot = dataSource.snapshot()
                snapshot.deleteAllItems()
                
                if !owner.favoriteMyCoffeeItems.isEmpty {
                    snapshot.appendSections([.favorite])
                    snapshot.appendItems(owner.favoriteMyCoffeeItems)
                }
                
                if !items.isEmpty {
                    snapshot.appendSections([.all])
                    snapshot.appendItems(items)
                }
                dataSource.apply(snapshot, animatingDifferences: false)
            }
            .store(in: &cancellables)
    }
    
    func setDataSource(for view: COFMyCoffeeView) {
        myCoffeeDataSource = COFMyCoffeeDataSource(tableView: view.tableView)
        view.dataSource = myCoffeeDataSource
    }
}

class COFMyCoffeeDataSource: UITableViewDiffableDataSource<COFMyCoffeeSection, COFMyCoffeeItemModel> {
    init(tableView: UITableView) {
        super.init(tableView: tableView) { tableView, indexPath, itemIdentifier in
            let cell = tableView.reuse(COFMyCoffeeTableViewCell.self, indexPath)
            cell.itemModel = itemIdentifier
            return cell
        }
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let snapshot = snapshot()
        let section = snapshot.sectionIdentifiers[section]
        return section.sectionTitle
    }
}

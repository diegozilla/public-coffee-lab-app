//
//  COFHomeViewController.swift
//  CoffeeLabFlow
//
//  Created by Diego Espinoza on 9/12/22.
//

import UIKit
import CoffeeLabBL
import Combine

public protocol COFMyCoffeeFlowCoordinator: AnyObject {
    func onAddTapped()
    func onRowTapped(id: UUID)
}

public class COFMyCoffeeViewController: UIViewController {

    private lazy var presenter = COFMyCoffeePresenter()
    private lazy var rootView = COFMyCoffeeView(presenter: self.presenter)
    
    private let myCoffeeRepositoryService: COFMyCoffeeRepositoryServiceType
    private weak var flowCoordinator: COFMyCoffeeFlowCoordinator?
    private var cancellables = Set<AnyCancellable>()

    public override func loadView() {
        view = rootView
    }
    
    public init(flowCoordinator: COFMyCoffeeFlowCoordinator,
                myCoffeeRepositoryService: COFMyCoffeeRepositoryServiceType) {
        self.flowCoordinator = flowCoordinator
        self.myCoffeeRepositoryService = myCoffeeRepositoryService
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
        setupItem()
        setupBindings()
        setupData()
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // TODO: - find a way to communicate the list update
        fetchMyCoffee()
    }
    
    private func setupBindings() {
        rootView.onRowTap
            .sink { [weak self] uuid in
                self?.flowCoordinator?.onRowTapped(id: uuid)
            }
            .store(in: &cancellables)
    }
    
    private func setupData() {
        fetchMyCoffee()
    }

    @MainActor private func fetchMyCoffee() {
        Task {
            do {
                let myCoffee = try await myCoffeeRepositoryService.fetch()
                let favoriteItems: [COFMyCoffeeItemModel] = myCoffee.filter(\.isFavorite).map {
                    .init(myCoffee: $0, category: .favorite)
                }
                let items: [COFMyCoffeeItemModel] = myCoffee.map {
                    .init(myCoffee: $0, category: .all)
                }
                presenter.favoriteMyCoffeeItems = favoriteItems
                presenter.myCoffeeItems = items
            } catch {
                presenter.error = error
            }
        }
    }

    private func setupItem() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add,
                                                            target: self,
                                                            action: #selector(onAddTap))
    }
}

// MARK: - Coordinator methods

extension COFMyCoffeeViewController {
    @objc private func onAddTap() {
        flowCoordinator?.onAddTapped()
    }
}

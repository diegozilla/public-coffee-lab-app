//
//  COFHomeView.swift
//  CoffeeLabFlow
//
//  Created by Diego Espinoza on 9/12/22.
//

import UIKit
import CoffeeLabUI
import Combine

internal class COFMyCoffeeView: UIView {
    
    // MARK: - Bindings
    
    var onRowTap: AnyPublisher<UUID, Never> { _onRowTap.eraseToAnyPublisher() }
    
    private let _onRowTap: PassthroughSubject<UUID, Never> = .init()
    
    // MARK: - Views
    
    lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .insetGrouped)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(COFMyCoffeeTableViewCell.self)
        tableView.delegate = self
        return tableView
    }()
    
    var dataSource: COFMyCoffeeDataSource? {
        didSet { tableView.dataSource = dataSource }
    }
    private let presenter: COFMyCoffeePresenter

    init(presenter: COFMyCoffeePresenter) {
        self.presenter = presenter
        super.init(frame: .zero)
        setupView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        tableView.attachToSafeArea(rootView: self)
        presenter.setDataSource(for: self)
        backgroundColor = .systemBackground
    }
}

extension COFMyCoffeeView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = presenter.sections[indexPath.section]
        switch section {
        case .favorite:
            _onRowTap.send(presenter.favoriteMyCoffeeItems[indexPath.item].uuid)
        case .all:
            _onRowTap.send(presenter.myCoffeeItems[indexPath.item].uuid)
        case .latest:
            fatalError("Not supported yet")
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

//
//  COFTabBarCoordinator.swift
//  CoffeeLabFlow
//
//  Created by Diego Espinoza on 9/12/22.
//

import UIKit
import CoffeeLabUI

open class COFTabBarCoordinator: UITabBarController, COFCoordinator {
    public func start() {
        fatalError("Implement COFTabBarCoordinator start method")
    }
}

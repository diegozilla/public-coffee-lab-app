//
//  COFNavigationCoordinator.swift
//  CoffeeLabFlow
//
//  Created by Diego Espinoza on 9/12/22.
//

import UIKit

open class COFNavigationCoordinator: UINavigationController, COFCoordinator {
    public func start() {
        fatalError("Implement COFNavigationCoordinator start method")
    }
}

//
//  Coordinator.swift
//  CoffeeLabFlow
//
//  Created by Diego Espinoza on 9/12/22.
//

import Foundation

public protocol COFCoordinator {
    func start()
}

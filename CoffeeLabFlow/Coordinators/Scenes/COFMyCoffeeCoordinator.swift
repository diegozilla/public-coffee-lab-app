//
//  COFMyCoffeeCoordinator.swift
//  CoffeeLabFlow
//
//  Created by Diego Espinoza on 10/12/22.
//

import UIKit
import CoffeeLabBL

final class COFMyCoffeeCoordinator: COFNavigationCoordinator {
    override func start() {
        let viewModel = COFCoffeeListViewModel(myCoffeeRepositoryService: COFMyCoffeeRepositoryService(),
                                               coordinator: self)
        let view = COFCoffeeListView(viewModel: viewModel)
        let viewController = COFCoffeeListViewController(viewModel: viewModel,
                                                               rootView: view)
        viewController.setTitle("My Coffee")
        viewController.setTabBarItem(title: "My Coffee",
                                     icon: UIImage(systemName: "cup.and.saucer"),
                                     selectedIcon: UIImage(systemName: "cup.and.saucer.fill"))
        setViewControllers([viewController], animated: false)
    }
}

extension COFMyCoffeeCoordinator: COFCoffeeListCoordinator {
    func onCreateCoffeeTap() {
        let viewModel = COFCoffeeCreateViewModel(myCoffeeRepositoryService: COFMyCoffeeRepositoryService(),
                                                 coordinator: self)
        let view = COFCoffeeCreateNameView(viewModel: viewModel)
        let viewController = COFCoffeeCreateNameViewController(viewModel: viewModel, rootView: view)
        viewController.setLargeTitle()
        viewController.setTitle("Name and Roast")
        pushViewController(viewController, animated: true)
    }

    func onCoffeeItemTap(id: UUID) {
        let viewModel = COFCoffeeCreateViewModel(coffeeId: id,
                                                 myCoffeeRepositoryService: COFMyCoffeeRepositoryService(),
                                                 coordinator: self)
        let view = COFCoffeeCreateNameView(viewModel: viewModel)
        let viewController = COFCoffeeCreateNameViewController(viewModel: viewModel, rootView: view)
        viewController.setLargeTitle()
        viewController.setTitle("Name and Roast")
        pushViewController(viewController, animated: true)
    }
}

extension COFMyCoffeeCoordinator: COFCoffeeCreateCoordinator {
    func onCreateNameContinue(viewModel: COFCoffeeCreateViewModelType) {
        let view = COFCoffeeCreateInformationView(viewModel: viewModel)
        let viewController = COFCoffeeCreateInformationViewController(viewModel: viewModel, rootView: view)
        viewController.setLargeTitle()
        viewController.setTitle("More Information")
        pushViewController(viewController, animated: true)
    }
    
    func onCreateInformationContinue(viewModel: COFCoffeeCreateViewModelType) {
        let view = COFCoffeeCreateImageView(viewModel: viewModel)
        let viewController = COFCoffeeCreateImageViewController(viewModel: viewModel, rootView: view)
        viewController.setLargeTitle()
        viewController.setTitle("Set an image")
        pushViewController(viewController, animated: true)
    }
    
    func onCreateImageContinue() {
        popToRootViewController(animated: true)
    }
}

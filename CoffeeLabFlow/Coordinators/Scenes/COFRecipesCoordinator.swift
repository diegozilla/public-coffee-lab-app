//
//  COFRecipesCoordinator.swift
//  CoffeeLabFlow
//
//  Created by Diego Espinoza on 13/12/22.
//

import UIKit

final class COFRecipesCoordinator: COFNavigationCoordinator {
    override func start() {
        let myRecipesViewController = COFMyRecipesViewController()
        myRecipesViewController.setTitle("My Recipes")
        myRecipesViewController.setTabBarItem(title: "My Recipes",
                                              icon: UIImage(systemName: "takeoutbag.and.cup.and.straw"),
                                              selectedIcon: UIImage(systemName: "takeoutbag.and.cup.and.straw.fill"))
        setViewControllers([myRecipesViewController], animated: false)
    }
}

//
//  COFHomeCoordinator.swift
//  CoffeeLabFlow
//
//  Created by Diego Espinoza on 10/12/22.
//

import UIKit

public class COFHomeCoordinator: COFTabBarCoordinator {
    public override func start() {
        let myCoffeeCoordinator = COFMyCoffeeCoordinator()
        myCoffeeCoordinator.setLargeTitleNavigation()
        myCoffeeCoordinator.start()
        
        let recipesCoordinator = COFRecipesCoordinator()
        recipesCoordinator.setLargeTitleNavigation()
        recipesCoordinator.start()

        setViewControllers([myCoffeeCoordinator, recipesCoordinator], animated: false)
    }
}

//
//  BaseViewModelType.swift
//  CoffeeLabFlow
//
//  Created by Diego Espinoza on 29/01/23.
//

import Foundation

protocol COFBaseViewModelType: AnyObject {
    func initialize()
}

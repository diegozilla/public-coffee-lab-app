//
//  PropertyBinding.swift
//  CoffeeLabFlow
//
//  Created by Diego Espinoza on 23/12/22.
//

import Foundation
import Combine

typealias BindableProperty<T> = CurrentValueSubject<T, Never>

@propertyWrapper
final class PropertyBinding<T> {
    private var subject: CurrentValueSubject<T, Never>
    
    var wrappedValue: T {
        get { subject.value }
        set { subject.send(newValue) }
    }
    
    var projectedValue: CurrentValueSubject<T, Never> {
        get { subject }
        set { subject = newValue }
    }
    
    init(wrappedValue: T) {
        self.subject = .init(wrappedValue)
    }
}

@propertyWrapper
final class DiffableProperty<T> {
    struct Diff<T> {
        let oldValue: T?
        let currentValue: T
    }

    private var subject: CurrentValueSubject<Diff<T>, Never>
    
    var wrappedValue: Diff<T> {
        get { subject.value }
        set { subject.send(newValue) }
    }
    
    var projectedValue: CurrentValueSubject<Diff<T>, Never> {
        get { subject }
        set { subject = newValue }
    }
    
    init(wrappedValue: Diff<T>) {
        subject = .init(wrappedValue)
    }
}

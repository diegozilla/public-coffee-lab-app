# Coffee-Lab-App (temporal name)

## Motivation:

I consider myself a big fan of coffee. Not only I love drinking a good cup, I also enjoy the preparing it. Making a good cup of coffee can be a difficult task, because depending on the coffee and the brewing method, you can get different results depending on the preparation.

Some of the variables or factors that can affect a results are:
- Water temperature
- Brewing method
- Grind size of the coffee
- Coffee/Water ratio

Having so many variables means that getting a good coffee will depend on multiple factors, which can be reproduced once they are known but many times there is a lot of experimentation. For example increasing the grind size of certain coffee might increase the sweetness or decreasing the water temperatura might give more acidic flavors.

What I want for this app is to help coffee enthusiasts, as myself, storing their results for each coffee and method they use. This way it will be easier to get a their desired coffee cup and also store the cups they didn't like that much, this way it can be improved next time.

## Architecture:

Currently the app contains 3 Cocoa Touch Frameworks:
- `CoffeeLabFlow`, which is the module where all the coffee related ViewControllers, Views and Presenters will be implemented.
- `CoffeeLabUI`, which contains the UI elements and components that will be used in the app.
- `CoffeeLabBL`, which contains the models, service and storage classes.

The navigation will be implemented through `Coordinators`, although instead of implementing a regular object with the `Coordinator` protocol, the Coordinator will be implemented by a ViewController (mainly subclassing `UITabBarController` and `UINavigationController`). The main reason for this is that by using a ViewController it is easier to avoid memory leaks and store the ViewControllers.

The main pattern of the app is MVVM and Combine for the reactive functionalities.

Another key component of the app is that it will use the new concurrency API which is async/await.

Finally the app is being build with accessibility in mind, so it will use most of the accessibility features available in the iOS platform.

## Considerations

- Currently the app is not using libraries or external frameworks but I intended to install the following:
    - R.swift (for generating typed images/localizable strings)
    - Swiftlint (for linting the code)
    - ACKLocalization (for localizable strings)
- The app works for iOS 15+

- Currently only the basic Coffee scenes are implemented.
- Currently there is no delete feature for the added coffee.
- The app is using core data, although it is not optimized to used background contexts at the moment.
- The image selection for the create/update coffee is not implemented yet (it just uses a mock image).
- This is a pet project but definitely i intended to make it work as it will help with the preparation of coffee as my hobby.

## Current status

There are 3 modules/scenes that needs to be implemented for this to work:
- Coffee list/input, so that a user can create a recipe with it
- Recipe creation, which will allow the user to select a coffee from the list, enter a preparation process and later will allow the user to replicate the process easily.
- A history list of the preparations, so that the user can see his/her results.

Currently only the first module: Coffee is working but it can be used so to check how the project structure will work.